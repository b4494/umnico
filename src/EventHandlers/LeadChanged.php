<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\Umnico\Utils\DataWrap;

class LeadChanged
{
    public function __invoke(DataWrap $data)
    {
        return new ChatDataChanged(
            $data->source(),
            $data->timestamp(),
        );
    }
}
