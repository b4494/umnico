<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\DataWrap;

class MessageIncoming
{
    public function __invoke(DataWrap $data)
    {
        return new InboxReceived(
            chat: $data->source(),
            participant: $data->customer(),
            message: $data->message(),
            externalItem: $this->socialPost($data),
            flags: $this->flags($data) ?: null,
            timestamp: $data->timestamp(),
        );
    }

    protected function socialPost(DataWrap $data): ?ExternalPlatformItem
    {
        if ($preview = data_get($data, 'message.preview', false)) {
            return new ExternalPlatformItem(
                url: data_get($preview, 'url'),
                extraData: Helpers::withoutEmptyStrings(['photo' => data_get($preview, 'photo')])
            );
        }

        return null;
    }

    protected function flags(DataWrap $data): int
    {
        $flags = 0;
        if (data_get($data, 'isNewCustomer', false)) {
            $flags |= InboxFlags::NEW_CHAT_CREATED;
        }
        if (data_get($data, 'message.preview', false)) {
            $flags |= InboxFlags::EXTERNAL_POST_COMMENT;
        }
        return $flags;
    }
}
