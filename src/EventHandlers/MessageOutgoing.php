<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\Entities\Helpers\Message;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class MessageOutgoing extends MessageIncoming
{
    public function __invoke(DataWrap $data)
    {
        $customId = $data['customId'] ?? $data->messageExternal()['customId'] ?? null;

        if (!$customId || $this->skipCustomId($customId)) {
            return null;
        }

        return new OutboxSent(
            chat: $data->source(),
            message: $data->message(),
            timestamp: $data->timestamp(),
        );
    }

    protected function skipCustomId(mixed $customId): bool
    {
        return $customId === Config::get('umnico_params.custom_id')
                // Skip messages that are not sent from umnico web app
            // TODO: make a configuration option
            || !Str::is('umnico-web-client-*', $customId);
    }
}
