<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\CustomEventOccurred;
use BmPlatform\Umnico\Utils\DataWrap;

class LeadChangedStatus
{
    public function __invoke(DataWrap $data)
    {
        return new CustomEventOccurred(
            chat: $data->source(),
            eventId: 'leadStatusChanged',
            timestamp: $data->timestamp(),
        );
    }
}