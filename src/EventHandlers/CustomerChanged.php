<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\ContactDataUpdated;
use BmPlatform\Umnico\Utils\DataTypeFactory;
use BmPlatform\Umnico\Utils\DataWrap;

class CustomerChanged
{
    public function __invoke(DataWrap $data)
    {
        return new ContactDataUpdated(
            contact: $data->customer(),
            timestamp: $data->timestamp()
        );
    }
}
