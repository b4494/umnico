<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Chat;
use Illuminate\Support\Facades\Config;

final class Utils
{
    /** @return array{0: int, 1: int} */
    public static function parseExternalId(string $externalId): array
    {
        return explode(':', $externalId, 2);
    }

    public static function resolveMessagingIdForMigratedChats(array $sources, string $integrationType, $leadId)
    {
        $sources = array_filter($sources, fn($source) => $source['type'] == 'message');
        if (empty($sources)) {
            throw new ErrorException(ErrorCode::ChatNotFound, 'Empty sources! LeadId: ' . $leadId);
        }
        if (count($sources) > 1) {
            if ($integrationType == 'telegram') {
                return self::retrieveSourceWithMinRealId($sources);
            }
            throw new ErrorException(
                ErrorCode::ChatNotFound,
                'Two sources with type "message" found for ' . $integrationType . ' on leadId: ' . $leadId
            );
        }
        return $sources[array_key_first($sources)];
    }

    public static function retrieveSourceWithMaxRealId(array $sourcesWithRealId): array
    {
        $source = $sourcesWithRealId[array_key_first($sourcesWithRealId)];
        foreach ($sourcesWithRealId as $src) {
            if ($src['realId'] > $source['realId']) {
                $source = $src;
            }
        }
        return $source;
    }

    public static function retrieveSourceWithMinRealId(array $sourcesWithRealId): array
    {
        $source = $sourcesWithRealId[array_key_first($sourcesWithRealId)];
        foreach ($sourcesWithRealId as $src) {
            if ($src['realId'] < $source['realId']) {
                $source = $src;
            }
        }
        return $source;
    }

    public static function unsetFields(array $target, array $fields): array
    {
        foreach ($fields as $field) {
            unset($target[$field]);
        }
        return $target;
    }

    public static function matchServer($apiUrl): ?string
    {
        foreach (Config::get('umnico.servers') as $id => $server) {
            if (preg_match("#^{$server['matcher']}$#", $apiUrl)) {
                return $id;
            }
        }

        return null;
    }

    public static function failIfOldStructure(Chat $chat): void
    {
        if (self::hasOldStructure($chat)) {
            throw new ErrorException(ErrorCode::ChatNotFound, 'Chat has old structure!');
        }
    }

    protected static function hasOldStructure(Chat $chat): bool
    {
        return isset($chat->getExtraData()[ExtraDataProps::LEAD_ID])
            || isset($chat->getExtraData()[ExtraDataProps::VIRTUAL_SOURCE_REAL_ID]);
    }
}
