<?php

namespace BmPlatform\Umnico\Utils;


enum UmnicoChannelType
{
//comment - комментарий к посту
//message - сообщение
//photo - комментарий к фотографии
//video - комментарий к видеозаписи
//market - комментарий к товару
    case comment;
    case message;
    case photo;
    case video;
    case market;
}
