<?php

namespace BmPlatform\Umnico\Utils;

class ExtraDataProps
{
    const CHAT_EXTERNAL_ID = 'chatExternalId';
    const LEAD_ID = 'leadId';
    const VIRTUAL_SOURCE_REAL_ID = 'currentRealId';
    const SOURCES = 'sources';
    const ADMIN_OPERATOR_ID = 'adminId';
    const TOKEN = 'token';
    const CONTACT_PROFILES = 'p';
    const CONTACT_LOGIN = 'l';
    const ACCESS_TO_SOURCES = 'accessToSources';
    const EXTERNAL_METADATA = 'metadataFromExternalSystem';
    const LEAD_DATA = 'ld';
    const SOURCE_REAL_ID = 'srd';
    const SOURCE_ID = 'sid';
}
