<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\Enums\MediaFileType;

enum UmnicoMediaFileType
{
    case photo;
    case doc;
    case video;
    case audio;
    // TODO: figure out what to do about the rest.
    case link;
    case sticker;
    case market;
    case mail;

    public static function convertToInternal($type): ?MediaFileType
    {
        return match ($type) {
            self::audio->name => MediaFileType::Audio,
            self::doc->name => MediaFileType::Document,
            self::photo->name => MediaFileType::Image,
            self::video->name => MediaFileType::Video,
            default => null,
        };
    }
}
