<?php

namespace BmPlatform\Umnico\Utils;

enum UmnicoStatusType: string
{
//active - активный тип
//completed - архивный успешный тип
//failed - архивный неуспешный тип
//ignore - тип «Не обращение»
    case active = 'active';
    case completed = 'completed';
    case failed = 'failed';
    case ignore = 'ignore';

    public static function isActive(string $type): bool
    {
        return $type === self::active->value;
    }
}
