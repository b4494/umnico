<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\AppHandler;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method sourceExternal(): array
 * @method messageExternal(): array
 * @method customerExternal(): array
 * @method managerExternal(): array
 * @method integrationExternal(): array
 * @method manager(): BmPlatform\Abstraction\DataTypes\Operator
 * @method source(): BmPlatform\Abstraction\DataTypes\Chat
 * @method message(): BmPlatform\Abstraction\DataTypes\MessageData
 * @method integration(): BmPlatform\Abstraction\DataTypes\MessengerInstance
 * @method customer(): BmPlatform\Abstraction\DataTypes\Contact
 * @method sourceInstance(): string
 * @method messageInstance(): string
 * @method customerInstance(): string
 * @method managerInstance(): string
 * @method integrationInstance(): string
 */
class DataWrap implements \ArrayAccess
{
    const DATA_KEY = 'data';
    const DATA_SOURCE_KEY = 'type';
    const DATA_SOURCE_API = 'set_from_api';
    const USE_LATEST_SOURCE = [ 'avito', 'mailbox' ];

    #[ArrayShape([
        'source' => 'null|SourceEntity',
        'message' => 'null|MessageEntity',
        'manager' => 'null|ManagerEntity',
        'contact' => 'null|ContactEntity',
        'integration' => 'null|IntegrationEntity',
    ])]
    protected array $entities;

    protected array $retrievedFromApi;

    #[ArrayShape([
        'id' => 'int',
        'identifier' => 'string',
        'leadId' => 'int',
        'realId' => 'int',
        'name' => 'string',
        'type' => 'string',
        'saId' => 'int',
        'sender' => 'string',
        'token' => 'string',
    ])]
    protected array $externalSource;

    #[ArrayShape([
        'leadId' => 'int',
        'messageId' => 'int',
        'message' => ['text' => 'null|string', 'attachments' => 'null|string'],
    ])]
    protected array $externalMessage;

    #[ArrayShape([
        'id' => 'int'
    ])]
    protected array $externalManager;

    #[ArrayShape([
        'id' => 'int',
        'name' => 'null|string',
        'login' => 'null|string',
        'email' => 'null|string',
        'address' => 'null|string',
        'phone' => 'null|string',
        'profiles' => 'null|array',
    ])]
    protected array $externalCustomer;

    #[ArrayShape([
        'id' => 'int'
    ])]
    protected array $externalIntegration;

    public function __construct(
        public readonly AppHandler $module,
        public readonly array $data,
        protected ?Carbon $timestamp = null,
    ) {
        //
    }

    public function timestamp(): Carbon
    {
        return $this->timestamp ?: now();
    }

    public function __call(string $name, array $arguments)
    {
        $entityName = explode('_', Str::snake($name))[0];
        $class = $this->getClass($entityName);
        $propertyName = $this->getPropertyName($entityName);
        $this->setProperty($propertyName);
        if (str_contains($name, 'External')) {
            return $this->$propertyName;
        }
        if (str_contains($name, 'Instance')) {
            return $class::getExternalId($this->$propertyName);
        }
        return $this->getEntity($class, $propertyName, $entityName);
    }

    private function getClass(string $entityName)
    {
        return 'BmPlatform\Umnico\Utils\Entities\\' . ucfirst($entityName) . 'Entity';
    }

    private function getEntity(string $class, string $propertyName, string $entityName): Arrayable
    {
        return $this->entities[$entityName] ?? $this->entities[$entityName] = (new $class(
            $this->module,
            $this
        ))->getMappedEntity();
    }

    private function getPropertyName(string $entityName)
    {
        return 'external' . ucfirst($entityName);
    }

    protected function setProperty($propertyName): void
    {
        if (!property_exists(self::class, $propertyName)) {
            throw new ErrorException(
                ErrorCode::UnexpectedServerError,
                'DataWrap doesn\'t have ' . $propertyName . ' property'
            );
        }
        if (isset($this->$propertyName)) {
            return;
        }
        if ($this->getDataSource() == self::DATA_SOURCE_API) {
            $this->$propertyName = $this->data[self::DATA_KEY];
            return;
        }
        $setter = 'set' . ucfirst($propertyName);
        $this->$setter();
    }

    protected function setExternalSource(): void
    {
        $leadData = $this->data['lead'] ?? $this->retrieveFromApi('getLead', [ $this->data['leadId'] ]);

        $leadData = Helpers::withoutEmptyStrings(
            Utils::unsetFields($leadData, ['socialAccount', 'customer', 'message'])
        );

        $integrationType = $this->data['message']['sa']['type'] ?? $this->data['lead']['socialAccount']['type'];

        $source = $this->resolveSource($integrationType, $this->data['leadId']);

        if ($integrationType === 'mailbox') {
            // add field subject for mail, assign email as id.
            $source['subject'] = $source['id'];
            $source['id'] = $source['identifier'];
        }

        $this->externalSource = match ($this->getDataSource()) {
            'message' => [
                'customerId' => $this->data['message']['sender']['customerId'] ?? $this->retrieveFromApi(
                        'getLead',
                        [ $this->data['leadId'] ]
                    )['customerId'],
                ExtraDataProps::LEAD_DATA => $leadData,
                ...$source
            ],

            'lead' => [
                'customerId' => $this->data['lead']['customer']['id'] ?? null,
                ExtraDataProps::LEAD_DATA => $leadData,
                ...$source
            ],

            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            )
        };

        $this->externalSource = [
            'leadId' => $this->data['leadId'],
            'integrationType' => $integrationType,
            ...$this->externalSource
        ];
    }

    protected function resolveSource(string $integrationType, $leadId): array
    {
        $currentSource = $this->data['message']['source'] ?? false;

        if (in_array($integrationType, self::USE_LATEST_SOURCE)) {
            $sources = $this->retrieveFromApi('getSourcesWithRealId', [ $leadId ]);

            $this->exitIfEmptySources($sources, $leadId);

            $currentSource
                ? $this->exitIfNotMaxRealId($sources, $currentSource, $leadId)
                : $currentSource = Utils::retrieveSourceWithMaxRealId($sources);

            return $currentSource;
        } elseif (($currentSource['type'] ?? null) == 'message' && $integrationType != 'telegram') {
            return $currentSource;
        }

        $sources = $this->retrieveFromApi('getSources', [ $leadId ]);

        $sources = array_filter($sources, fn ($source) => $source['type'] == 'message');

        $this->exitIfEmptySources($sources, $leadId);

        // This for private telegram, not bot
        if ($integrationType == 'telegram') {
            $sources = array_filter($sources, fn ($source) => isset($source['realId']));

            $this->exitIfEmptySources($sources, $leadId);

            return Utils::retrieveSourceWithMinRealId($sources);
        } elseif (count($sources) > 1) {
            // This means that we have received a comment, so we will try to find source of type message with same name
            if (($currentSource['type'] ?? null) === 'comment' && ($value = Arr::first($sources, fn ($arr) => $arr['name'] == $currentSource['name']))) {
                return $value;
            }

            throw new IgnoreProcessingException(
                ErrorCode::UnexpectedData,
                'Event ' . $this->data[self::DATA_SOURCE_KEY],
                'Two sources with type "message" found for ' . $integrationType . ' on leadId: ' . $leadId
            );
        }

        return $sources[array_key_first($sources)];
    }

    protected function exitIfNotMaxRealId(array $sources, array $source, $leadId): void
    {
        $maxRealId = max(array_column($sources, 'realId'));
        if ($source['realId'] == $maxRealId) {
            return;
        }
        throw new IgnoreProcessingException(
            ErrorCode::ChatNotFound,
            'Event ' . $this->data[self::DATA_SOURCE_KEY],
            __METHOD__ . ' LeadId: ' . $leadId
        );
    }

    protected function exitIfEmptySources(array $sources, $leadId)
    {
        if (empty($sources)) {
            throw new IgnoreProcessingException(
                ErrorCode::ChatNotFound,
                'Event ' . $this->data[self::DATA_SOURCE_KEY],
                __METHOD__ . ' LeadId: ' . $leadId
            );
        }
    }

    protected function setExternalMessage(): void
    {
        $this->externalMessage = match ($this->getDataSource()) {
            'message' => array_merge($this->data['message'], [
                'leadId' => $this->data['leadId'],
            ]),

            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            )
        };
    }

    protected function setExternalManager(): void
    {
        $this->externalManager = match ($this->getDataSource()) {
            'message', 'lead' => [
                'id' => $this->data['lead']['userId'] ?? $this->retrieveFromApi(
                        'getLead',
                        [$this->data['leadId']]
                    )['userId']
            ],
            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            )
        };
    }

    protected function setExternalCustomer(): void
    {
        $this->externalCustomer = match ($this->getDataSource()) {
            'message' => [
                'id' => $this->data['message']['sender']['customerId'] ?? $this->retrieveFromApi(
                        'getLead',
                        [$this->data['leadId']]
                    )['customerId']
            ],
            'customer' => $this->data['customer'],
            'lead' => $this->data['lead']['customer'] ?? [
                'id' => $this->retrieveFromApi(
                    'getLead',
                    [$this->data['leadId']]
                )['customerId']
            ],
            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            )
        };
    }

    protected function setExternalIntegration(): void
    {
        $this->externalIntegration = match ($this->getDataSource()) {
            'message' => ['id' => $this->data['message']['sa']['id']],
            'lead' => ['id' => $this->data['lead']['socialAccount']['id']],
            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            )
        };
    }

    protected function retrieveFromApi(string $name, array $arguments)
    {
        return $this->retrievedFromApi[$name] ?? $this->retrievedFromApi[$name] = $this->module->getApiCommands(
        )->$name(
            ...$arguments
        );
    }

    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset): mixed
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset): void
    {
        // TODO: Implement offsetUnset() method.
    }

    public function getDataSource(): string
    {
        return explode('.', $this->data[self::DATA_SOURCE_KEY])[0];
    }
}
