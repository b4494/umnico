<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Umnico\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\Umnico\Utils\ExtraDataProps;

class CustomerFromWebhook extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = ['id', 'name|login'];
    const FIELDS_WITH_DEFAULTS = [
        'name' => null,
        'login' => null,
        'email' => null,
        'address' => null,
        'phone' => null,
        'comment' => null,
        'profiles' => null,
    ];

    protected function setEntity()
    {
        $this->entity = new Contact(
            externalId: $this->externalEntity['id'],
            name: $this->externalEntity['name'] ?? $this->externalEntity['login'],
            phone: $this->externalEntity['phone'],
            email: $this->externalEntity['email'],
            avatarUrl: $this->externalEntity['avatar'],
            extraFields: [
                'address' => $this->externalEntity['address'],
            ],
            messengerId: null,
            extraData: $this->getExtraData()
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    protected function setExtraData(): void
    {
        $this->extraData = [
            ExtraDataProps::CONTACT_PROFILES => $this->externalEntity['profiles'],
            ExtraDataProps::CONTACT_LOGIN => $this->externalEntity['login'],
        ];
    }
}
