<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\Entities\Contracts\EntityWithAdditionalApiCalls;
use BmPlatform\Umnico\Utils\ExtraDataProps;

class CustomerFromApi extends EntityWithAdditionalApiCalls
{
    const REQUIRED_PRESET_FIELDS = ['id'];
    const FIELDS_WITH_DEFAULTS = [
        'name' => null,
        'login' => null,
        'email' => null,
        'address' => null,
        'phone' => null,
        'comment' => null,
        'profiles' => null,
    ];

    protected function setEntity()
    {
        $contact = $this->externalEntity[self::API_DATA_KEY];
        $this->entity = new Contact(
            externalId: $this->externalEntity['id'],
            name: $contact['name'] ?? $contact['login'],
            phone: $contact['phone'],
            email: $contact['email'],
            avatarUrl: $contact['avatar'],
            extraFields: [
                'address' => $contact['address'],
            ],
            messengerId: null,
            extraData: $this->getExtraData()
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    public function getEntityFromApi(): array
    {
        return $this->module->getApiCommands()->getContact($this->externalEntity['id']);
    }

    protected function setExtraData(): void
    {
        $this->extraData = [
            ExtraDataProps::CONTACT_PROFILES => $this->externalEntity[self::API_DATA_KEY]['profiles'],
            ExtraDataProps::CONTACT_LOGIN => $this->externalEntity[self::API_DATA_KEY]['login'],
        ];
    }
}
