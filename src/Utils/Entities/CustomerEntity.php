<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\AppHandler;
use BmPlatform\Umnico\Utils\DataWrap;

class CustomerEntity
{
    protected CustomerFromApi|CustomerFromWebhook $contact;

    public function __construct(protected AppHandler $module, protected readonly DataWrap $data)
    {
        if (count($data->customerExternal()) > 1) {
            $this->contact = new CustomerFromWebhook($this->module, $data);
        } else {
            $this->contact = new CustomerFromApi($this->module, $data);
        }
    }

    public static function getExternalId(array $externalEntity)
    {
        return CustomerFromWebhook::getExternalId($externalEntity);
    }

    public function __call(string $name, array $arguments)
    {
        return $this->contact->$name();
    }
}
