<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Umnico\Utils\Entities\Contracts\EntityWithAdditionalApiCalls;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\ExtraDataProps;

class ManagerEntity extends EntityWithAdditionalApiCalls
{
    const REQUIRED_PRESET_FIELDS = ['id', 'name|login'];
    const FIELDS_WITH_DEFAULTS = [
        'sources' => null,
        'metadata' => null,
    ];

    protected function setEntity()
    {
        $this->entity = new Operator(
            externalId: $this->externalEntity['id'],
            name: $this->externalEntity['name'] ?? $this->externalEntity['login'],
            email: $this->externalEntity['login'],
            extraData: $this->getExtraData(),
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    public function getEntityFromApi(): array
    {
        return $this->module->getApiCommands()->getOperator($this->externalEntity['id']);
    }

    protected function setExtraData(): void
    {
        $this->extraData = [
            ExtraDataProps::ACCESS_TO_SOURCES => $this->externalEntity['sources'],
            // if the operator was imported from external system, we're gonna have info about the system
            ExtraDataProps::EXTERNAL_METADATA => $this->externalEntity['metadata'],
        ];
    }
}
