<?php

namespace BmPlatform\Umnico\Utils\Entities\Contracts;

use Illuminate\Contracts\Support\Arrayable;

interface MapsToInternalEntity
{
    public static function getExternalId(array $externalEntity): ?string;
    public function getMappedEntity(): Arrayable;
}
