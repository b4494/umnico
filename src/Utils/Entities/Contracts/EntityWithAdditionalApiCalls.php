<?php

namespace BmPlatform\Umnico\Utils\Entities\Contracts;

use BmPlatform\Umnico\Utils\Entities\Contracts\BaseEntity;
use Illuminate\Contracts\Support\Arrayable;

abstract class EntityWithAdditionalApiCalls extends BaseEntity implements GetsEntityFromApi
{
    const API_DATA_KEY = 'api_data';

    public function getMappedEntity(): Arrayable
    {
        if (isset($this->entity)) {
            return $this->entity;
        }
        $this->externalEntity[self::API_DATA_KEY] = $this->getEntityFromApi();
        $this->validate();
        $this->setDefaults();
        $this->setEntity();
        return $this->entity;
    }

    public function setExternalEntity(array $externalEntity)
    {
        $this->externalEntity = $externalEntity;
    }
}
