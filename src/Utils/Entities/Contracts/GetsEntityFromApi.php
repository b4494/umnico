<?php

namespace BmPlatform\Umnico\Utils\Entities\Contracts;

interface GetsEntityFromApi
{
    public function getEntityFromApi(): array;
}
