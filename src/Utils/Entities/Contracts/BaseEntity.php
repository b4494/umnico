<?php

namespace BmPlatform\Umnico\Utils\Entities\Contracts;

use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\AppHandler;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\Entities\Helpers\SetsDefaults;
use BmPlatform\Umnico\Utils\Entities\Helpers\ValidatesRequiredPresetFields;
use Illuminate\Contracts\Support\Arrayable;

abstract class BaseEntity implements MapsToInternalEntity
{
    use ValidatesRequiredPresetFields, SetsDefaults;

    protected array $externalEntity;
    protected array $extraData;

    const PRIMARY_KEY = 'id';
    // value-only
    const REQUIRED_PRESET_FIELDS = [];
    // key => value pairs, nested keys using dot notation are allowed
    const FIELDS_WITH_DEFAULTS = [];
    // value-only, using dot notation to represent nested keys
    const REQUIRED_PRESET_NESTED_FIELDS = [];

    protected Arrayable $entity;

    public function __construct(protected AppHandler $module, protected readonly DataWrap $data)
    {
        $entityName = lcfirst(str_replace('Entity', '', last(explode('\\', static::class)))) . 'External';
        $this->externalEntity = $data->$entityName();
    }

    public function getMappedEntity(): Arrayable
    {
        if (isset($this->entity)) {
            return $this->entity;
        }
        $this->validate();
        $this->setDefaults();
        $this->setEntity();
        return $this->entity;
    }

    public static function getExternalId(array $externalEntity): ?string
    {
        $id = data_get($externalEntity, static::PRIMARY_KEY);
        if (!$id) {
            return null;
        }
        if ($parentObjectId = static::getParentObjectId($externalEntity)) {
            $parentObjectId = ':' . $parentObjectId;
        }
        return $id . $parentObjectId;
    }

    abstract protected function setEntity();

    public function getExtraData(): array
    {
        if (!isset($this->extraData)) {
            $this->setExtraData();
        }
        return Helpers::withoutEmptyStrings($this->extraData);
    }

    abstract protected function setExtraData(): void;

    // Let's say we are working with telegram
    // Channel id would be parent object id for chat; Telegram bot id would be parent object id for channel.
    abstract protected static function getParentObjectId(array $externalEntity): ?string;
}
