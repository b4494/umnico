<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Umnico\Utils\Entities\Contracts\BaseEntity;
use Illuminate\Support\Facades\Config;

class IntegrationEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = ['id', 'type', 'login'];

    protected function setEntity()
    {
        $this->entity = new MessengerInstance(
            externalType: $this->externalEntity['type'],
            externalId: self::getExternalId($this->externalEntity),
            name: $this->externalEntity['login'],
            messengerId: match ($this->externalEntity['type']) {
                'telebot' => substr($this->externalEntity['url'], strlen('https://t.me/')),
                'viber_bot' => $this->externalEntity['url'],
                'vk_group' => $this->externalEntity['identifier'],
                default => null,
            },
            internalType: MessengerType::tryFrom(
                Config::get(
                    'umnico.messenger_types.' . $this->externalEntity['type'] . '.internal_type'
                )
            ),
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    protected function setExtraData(): void
    {
        $this->extraData = [];
    }
}
