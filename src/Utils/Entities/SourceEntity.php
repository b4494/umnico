<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Abstraction\Interfaces\Chat as ChatInterface;
use BmPlatform\Umnico\Utils\Utils;

class SourceEntity extends BaseEntity
{
    const USE_REAL_ID = ['avito', 'mailbox'];
    const PRIMARY_KEY = 'leadId';
    const REQUIRED_PRESET_FIELDS = ['leadId', 'id'];
    const FIELDS_WITH_DEFAULTS = [
        ExtraDataProps::LEAD_DATA => null,
    ];

    protected function setEntity()
    {
        $this->entity = new Chat(
            externalId: self::getExternalId($this->externalEntity),
            messengerInstance: $this->data->integrationInstance(),
            contact: $this->data->customerInstance(),
//            operator: $this->data->managerInstance(),
            messengerId: $this->externalEntity['id'],
            extraData: $this->getExtraData(),
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        if (in_array($externalEntity['integrationType'], self::USE_REAL_ID)) {
            return $externalEntity['realId'];
        }
        return null;
    }

    public static function getLeadId(ChatInterface $chat): string
    {
        return Utils::parseExternalId($chat->getExternalId())[0];
    }

    public static function getIdForMessageSending(ChatInterface $chat): string
    {
        if (in_array($chat->getMessengerInstance()->getExternalType(), self::USE_REAL_ID)) {
            return Utils::parseExternalId($chat->getExternalId())[1];
        }
        return $chat->getExtraData()[ExtraDataProps::SOURCE_REAL_ID] ??
            $chat->getExtraData()[ExtraDataProps::SOURCE_ID] ??
            throw new ErrorException(ErrorCode::DataMissing, 'Chat\'s extra data must contain either real_id or id.');
    }

    protected function setExtraData(): void
    {
        if (isset($this->externalEntity['realId'])) {
            $idForMessageSending = [ExtraDataProps::SOURCE_REAL_ID => $this->externalEntity['realId']];
        } else {
            $idForMessageSending = [ExtraDataProps::SOURCE_ID => $this->externalEntity['id']];
        }
        $this->extraData = [
            ExtraDataProps::LEAD_DATA => $this->externalEntity[ExtraDataProps::LEAD_DATA],
            ...$idForMessageSending,
        ];
    }
}
