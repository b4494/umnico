<?php

namespace BmPlatform\Umnico\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\UmnicoMediaFileType;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class MessageEntity extends BaseEntity
{
    const PRIMARY_KEY = 'leadId';
    const REQUIRED_PRESET_FIELDS = ['leadId', 'messageId', 'message'];
    const FIELDS_WITH_DEFAULTS = [
        'name' => null,
        'login' => null,
        'email' => null,
        'address' => null,
        'phone' => null,
        'comment' => null,
        'profiles' => null,
        'message.attachments' => null,
        'message.text' => null,
    ];
    const REQUIRED_PRESET_NESTED_FIELDS = [
        'message.text|message.attachments'
    ];

    public function getMappedEntity(): Arrayable
    {
        if (isset($this->entity)) {
            return $this->entity;
        }
        try {
            $this->validate();
        } catch (ErrorException $e) {
            // NOTE: this is a dirty fix for dirty webhook on inline buttons
            $requiredAreMissing = !($this->externalEntity['message']['text'] ?? $this->externalEntity['message']['attachments'] ?? null);
            if($requiredAreMissing) {
                throw new IgnoreProcessingException(
                    ErrorCode::DataMissing,
                    'Event ' . $this->data[DataWrap::DATA_SOURCE_KEY],
                    'webhook on inline buttons lacks message payload'
                );
            }
            throw $e;
        }
        $this->setDefaults();
        $this->setEntity();
        return $this->entity;
    }

    protected function setEntity()
    {
        $attachments = $this->attachments($this->externalEntity['message']['attachments']);
        $text = $this->externalEntity['message']['text'];
        if (empty($attachments) && empty($text)) {
            // this is a WH only entity so this will always be caught in the callback controller.
            throw new IgnoreProcessingException(ErrorCode::DataMissing, 'Message', 'Both text and attachments are empty.');
        }
        $this->entity = new MessageData(
            externalId: $this->data->messageInstance(),
            text: $text,
            attachments: $attachments,
            date: isset($this->externalEntity['datetime']) ? Carbon::parse($this->externalEntity['datetime']) : null,
            extraData: $this->getExtraData(),
        );
    }

    protected function attachments(?array $attachments): ?array
    {
        if (!$attachments) {
            return null;
        }
        return collect($attachments)
            ->filter(function ($attachment) {
                return (!is_null(UmnicoMediaFileType::convertToInternal(data_get($attachment, 'type'))) && data_get(
                        $attachment,
                        'url',
                        false
                    ));
            })
            ->map(fn($attachment) => new MediaFile(
                type: UmnicoMediaFileType::convertToInternal($attachment['type']),
                url: $attachment['url'],
            ))->all() ?: null;
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return $externalEntity['messageId'];
    }

    protected function setExtraData(): void
    {
        $this->extraData = [ExtraDataProps::CHAT_EXTERNAL_ID => $this->data->sourceInstance()];
    }
}
