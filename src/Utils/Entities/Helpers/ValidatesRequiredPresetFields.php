<?php

namespace BmPlatform\Umnico\Utils\Entities\Helpers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use Illuminate\Support\Facades\Log;

trait ValidatesRequiredPresetFields
{
    protected function validate(): void
    {
        $this->validateNestedFields();
        $this->validateRequiredPresetFieldsPresence();
    }

    protected function validateNestedFields(): void
    {
        $fields = $this->getNestedRequiredFields();
        foreach ($fields as $field) {
            $found = false;
            foreach ($field as $orField) {
                if (data_get($this->externalEntity, $orField)) {
                    $found = true;
                }
            }
            if (!$found) {
                throw new IgnoreProcessingException(
                    ErrorCode::DataMissing,
                    'Construction of ' . static::class,
                    'Fields ' . implode(',', $field) . ' are missing in ' . static::class
                );
            }
        }
    }

    protected function getNestedRequiredFields(): array
    {
        return $this->getFields(static::REQUIRED_PRESET_NESTED_FIELDS);
    }

    protected function validateRequiredPresetFieldsPresence(): void
    {
        $fields = $this->getFields(static::REQUIRED_PRESET_FIELDS);
        $filteredEntity = array_filter($this->externalEntity);
        foreach ($fields as $field) {
            $fieldIsNotSetOnEntity = count(array_intersect_key(array_flip($field), $filteredEntity)) == 0;
            if ($fieldIsNotSetOnEntity) {
                $missingFields = implode(',', $field);
                throw new IgnoreProcessingException(
                    ErrorCode::DataMissing,
                    'Construction of ' . static::class,
                    'Fields ' . $missingFields . ' are missing in ' . static::class
                );
            }
        }
    }

    protected function getFields(?array $fields): array
    {
        $notParsedFields = $fields ?? [];
        $fields = [];
        foreach ($notParsedFields as $field) {
            $fields[] = explode('|', $field);
        }
        return $fields;
    }
}
