<?php

namespace BmPlatform\Umnico\Utils\Entities\Helpers;

trait SetsDefaults
{
    protected function setDefaults()
    {
        $fields = $this->getFieldsWithDefaults();
        foreach ($fields as $field => $defaultValue) {
            data_set($this->externalEntity, $field, data_get($this->externalEntity, $field, $defaultValue));
        }
    }

    protected function getFieldsWithDefaults(): array
    {
        return static::FIELDS_WITH_DEFAULTS ?? [];
    }
}
