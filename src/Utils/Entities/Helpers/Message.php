<?php

namespace BmPlatform\Umnico\Utils\Entities\Helpers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessageStatus;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use Illuminate\Support\Facades\Config;

class Message
{
    public static function needsProcessing(array $externalMessage): bool
    {
        return !(static::isOurs($externalMessage));
    }

    private static function isOurs(array $externalMessage): bool
    {
        if (data_get($externalMessage, 'customId') === Config::get('umnico_params.custom_id')) {
            return true;
        }
        return false;
    }
}
