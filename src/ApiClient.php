<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Exceptions\ValidationException;
use BmPlatform\Support\Http\HttpClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\MessageBag;
use Psr\Http\Message\ResponseInterface;

class ApiClient extends HttpClient
{
    public function __construct(
        public string $domain,
        public string $token,
        array $options = [],
        public string $tokenType = 'bearer'
    ) {
        parent::__construct($options);
    }

    // TODO: we did not have use for it yet, but let's not remove it for future.
    public function all(string $path, ?int $cursor, int $limit = 200): IterableData
    {
        $options['query']['limit'] = $limit;
        $options['query']['offset'] = $cursor = $cursor ?: 0;

        $response = $this->get($path, $options);

        return new IterableData($response, count($response) == $limit ? $cursor + $limit : null);
    }

    protected function defaultConfig(): array
    {
        return [
            'base_uri' => $this->domain . '/v1.3/',
            RequestOptions::HEADERS => [
                'Authorization' => $this->tokenType.' ' . $this->token,
                'Accept' => 'application/json',
            ],
            RequestOptions::CONNECT_TIMEOUT => 3,
            RequestOptions::TIMEOUT => 60,
        ];
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function processResponse(ResponseInterface $response): mixed
    {
        if (500 <= $code = $response->getStatusCode()) {
            throw $this->handleServerError($code);
        }

        try {
            if ($data = $response->getBody()->getContents()) {
                return $this->parseData($data, $code);
            } else {
                return $this->handleEmptyResponse($code);
            }
        }

        finally {
            $response->getBody()->close();
        }
    }

    private function handleServerError(int $code): ErrorException
    {
        return match ($code) {
            502 => new ErrorException(ErrorCode::ServiceUnavailable),
            default => new ErrorException(ErrorCode::InternalServerError, $code),
        };
    }

    private function handleEmptyResponse(int $code): bool
    {
        return match (true) {
            $code < 400 => true,
            $code == 403 => throw new ErrorException(ErrorCode::AuthenticationFailed, 'Unknown error'),
            default => throw new ErrorException(ErrorCode::UnexpectedServerError, $code),
        };
    }

    private function parseData(string $data, int $code)
    {
        try {
            $data = json_decode($data, true, 512, JSON_INVALID_UTF8_IGNORE | JSON_THROW_ON_ERROR);
        } catch (\JsonException) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Failed to parse json');
        }

        if (empty($data['errors']) && $code < 400) {
            return $data ?? true;
        }

        $errorMessage = $data['errors'][0] ?? $data['description'] ?? $data['error'] ?? 'Unknown error';

        if (is_array($errorMessage)) $errorMessage = json_encode($errorMessage, JSON_UNESCAPED_UNICODE);

        throw match ($code) {
            // TODO: check what umnico gives us on validation error
            422 => new ValidationException(new MessageBag($data['errors'])),
            400 => new ErrorException(ErrorCode::BadRequest, $errorMessage),
            403 => match ($errorMessage) {
                'Forbidden: bot was blocked by the user' => new ErrorException(ErrorCode::ChatIsBanned, $errorMessage),
                'Account is blocked' => new ErrorException(ErrorCode::AccountDisabled, $errorMessage),
                default => new ErrorException(ErrorCode::AuthenticationFailed, $errorMessage),
            },
            401 => new ErrorException(ErrorCode::AuthenticationFailed, $errorMessage),
            404 => new ErrorException(ErrorCode::NotFound, $errorMessage),
            default => new ErrorException(ErrorCode::UnexpectedServerError, $errorMessage),
        };
    }
}
