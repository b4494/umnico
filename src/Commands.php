<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\InlineButton;
use BmPlatform\Abstraction\DataTypes\InlineUrlButton;
use BmPlatform\Abstraction\DataTypes\QuickReply;
use BmPlatform\Abstraction\Enums\QuickReplyType;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTags;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\Commands\UpdatesContactData;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Umnico\Commands\MessageCommands;
use BmPlatform\Umnico\Commands\OperatorCommands;
use BmPlatform\Umnico\Commands\TagCommands;

class Commands implements CommandsInterface, SendsSystemMessages, UpdatesContactData, TransfersChatToOperator,
                          SupportsChatTags
{
    use MessageCommands, OperatorCommands, TagCommands;

    public function __construct(public readonly AppHandler $module)
    {
    }

    public function updateContactData(UpdateContactDataRequest $request): ?Contact
    {
        $resp = $this->module->getApiClient()->put('customers/:customerId', [
            'params' => ['customerId' => $request->contact->getExternalId()],
            'json' => $request->data,
        ]);

        return new Contact(
            externalId: $resp['id'],
            name: $resp['name'],
            phone: $resp['phone'],
            email: $resp['email'],
            avatarUrl: $resp['avatar'],
            extraFields: [
                'address' => $resp['address'],
            ]
        );
    }

//    public function closeChatTicket(Chat $chat): void
//    {
//        Utils::failIfOldStructure($chat);
//        $currentLead = $this->getCurrentLead($chat);
//        $statuses = $this->module->getApiCommands()->getStatuses();
//        $currentLeadStatus = $this->getCurrentLeadStatus($currentLead, $statuses);
//
//        if (!UmnicoStatusType::isActive(data_get($currentLeadStatus, 'type'))) {
//            throw new ErrorException(ErrorCode::ChatTicketAlreadyClosed, 'Chat ticket is already closed.');
//        }
//
//        // Assign to admin if no operator provided
//        if (!data_get($currentLead, 'userId')) {
//            $currentLead['userId'] = data_get($this->module->user->getExtraData(), ExtraDataProps::ADMIN_OPERATOR_ID);
//        }
//
//        $completedStatus = $this->getCompletedStatus($statuses);
//
//        $this->module->getApiClient()->put('leads/:id', [
//            'params' => ['id' => data_get($currentLead, 'id')],
//            'json' => ['statusId' => data_get($completedStatus, 'id'), 'userId' => $currentLead['userId']],
//        ]);
//
//        Event::dispatch(new ChatTicketClosed($this->module->user, $chat->getExternalId(), timestamp: Carbon::now()));
//    }

//    protected function getCurrentLead(Chat $chat): ?array
//    {
//        return $this->module->getApiCommands()->getLead(SourceEntity::getLeadId($chat));
//    }
//
//    protected function getCurrentLeadStatus(?array $currentLead, ?array $statuses): ?array
//    {
//        $statusId = data_get($currentLead, 'statusId');
//        if (!$statusId) {
//            throw new ErrorException(ErrorCode::DataMissing, 'Can not get current lead\'s status id');
//        }
//        return $this->module->getApiCommands()->getStatus($statusId, $statuses);
//    }
//
//    protected function getCompletedStatus(?array $statuses): array
//    {
//        $completedStatus = $this->module->getApiCommands()->getStatusByType(UmnicoStatusType::completed, $statuses);
//        if (!$completedStatus) {
//            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not get completed status');
//        }
//        return $completedStatus;
//    }
    public function makeReplyMarkup(string $messengerType, array $quickReplies, array $inlineButtons): ?array
    {
        if ($messengerType !== 'telebot') return null;

        return match (true) {
            !!$inlineButtons => $this->makeInlineKeyboardMarkup($inlineButtons),
            !!$quickReplies => $this->makeReplyKeyboardMarkup($quickReplies),
            default => [ 'remove_keyboard' => true ],
        };
    }

    protected function makeInlineKeyboardMarkup(array $inlineButtons): array
    {
        return [
            'inline_keyboard' => array_map(fn ($row) => array_map(fn (InlineButton $b) => [
                'text' => $b->text,
                ...($b instanceof InlineUrlButton ? [ 'url' => $b->url ] : [ 'callback_data' => $b->payload ]),
            ], $row), $inlineButtons),
        ];
    }

    protected function makeReplyKeyboardMarkup(array $quickReplies): array
    {
        return [
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'keyboard' => array_map(fn ($row) => array_map(fn (QuickReply $b) => [
                'text' => $b->text,
                ...match ($b->type) {
                    QuickReplyType::Location => [ 'request_location' => true ],
                    QuickReplyType::Phone => [ 'request_contact' => true ],
                    QuickReplyType::Text => [],
                },
            ], $row), $quickReplies)
        ];
    }
}
