<?php

namespace BmPlatform\Umnico;

use Illuminate\Support\Facades\Config;
use BmPlatform\Umnico\Utils\UmnicoStatusType;
use Illuminate\Support\Arr;
use JetBrains\PhpStorm\ArrayShape;

class ApiCommands
{
    public function __construct(public readonly ApiClient $apiClient)
    {
        //
    }

    #[ArrayShape([
        'messageId' => 'int',
        'sa' => 'array',
        'sender' => 'array',
        'source' => 'array',
        'message' => ['text' => 'string'],
        'incoming' => 'bool',
        'channels' => ['id' => 'int', 'transports' => 'array'],
    ])]
    public function sendMessage(string $leadId, string $sourceRealId, int $operatorId, array $payload, ?array $replyMarkup = null): array
    {
        $additionalPayload = [
            'source' => $sourceRealId,
            'userId' => $operatorId,
        ];

        if ($customId = Config::get('umnico_params.custom_id')) {
            $additionalPayload['customId'] = $customId;
        }

        if ($replyMarkup) $additionalPayload['replyMarkup'] = $replyMarkup;

        return $this->apiClient->post('messaging/:lead-id/send', [
            'params' => ['lead-id' => $leadId],
            'json' => ['message' => $payload] + $additionalPayload
        ]);
    }

    public function uploadMedia(array $multipart): array
    {
        return $this->apiClient->post('messaging/upload', [
            'multipart' => $multipart
        ]);
    }

    public function addTagToLead($leadId, string $tagValue)
    {
        return $this->apiClient->post('tags/:leadId/:tag', ['params' => ['leadId' => $leadId, 'tag' => $tagValue]]);
    }

    public function deleteTagFromLead($leadId, string $tagValue)
    {
        return $this->apiClient->delete('tags/:leadId/:tag', ['params' => ['leadId' => $leadId, 'tag' => $tagValue]]);
    }

    public function changeLead($id, array $values)
    {
        return $this->apiClient->put('leads/:id', ['params' => ['id' => $id], 'json' => $values]);
    }

    public function transferLeadToOperatorAndAcceptIfNeeded($id, array $values)
    {
        return $this->apiClient->put('leads/:id/accept', ['params' => ['id' => $id], 'json' => $values]);
    }

    #[ArrayShape([
        'id' => 'int',
        'phone' => 'string',
        'comment' => 'string',
        'custom_fields' => 'array',
        'channels' => ['id' => 'int', 'transports' => 'array'],
    ])]
    public function client($id): array
    {
        return $this->apiClient->get('clients/:id', ['params' => compact('id')]);
    }

    #[ArrayShape([
        'id' => 'int',
        'type' => 'string',
        'name' => 'string',
        'order' => 'int',
        'color' => 'string'
    ])]
    public function getStatus($id, ?array $statuses = []): ?array
    {
        if (empty($statuses)) {
            return $this->getEntity($this->getStatuses(), 'id', $id);
        }
        return $this->getEntity($statuses, 'id', $id);
    }

    public function getStatusByType(UmnicoStatusType $statusType, ?array $statuses = []): ?array
    {
        if (empty($statuses)) {
            return $this->getEntity($this->getStatuses(), 'type', $statusType->value);
        }
        return $this->getEntity($statuses, 'type', $statusType->value);
    }

    public function getStatuses(): array
    {
        return $this->apiClient->get('statuses');
    }

    #[ArrayShape([
        'id' => 'string',
        'realId' => 'int',
        'name' => 'string',
        'type' => 'string',
        'saId' => 'int',
        'sender' => 'string',
        'token' => 'string',
        'identifier' => 'string',
    ])]
    public function getSource($leadId, $saId): array
    {
        $sources = $this->getSources($leadId);
        if (count($sources) == 1) {
            return data_get($sources, 0);
        }
        foreach ($sources as $source) {
            if ($this->isTargetSource($source, $saId)) {
                return $source;
            }
        }
        return [];
    }

    // TODO: I hardcoded the channel type here on the premise of not having any other channel types supported by botmarketing
    protected function isTargetSource(array $source, int|string $saId, string $channelType = 'message'): bool
    {
        return (data_get($source, 'saId') == $saId) && (data_get($source, 'type') == $channelType);
    }

    public function getSourcesWithRealId($leadId): array
    {
        return array_filter($this->getSources($leadId), fn($src) => isset($src['realId']));
    }

    public function getSources($leadId, $key = null, $value = null): array
    {
        $sources = $this->apiClient->get('messaging/:id/sources', ['params' => ['id' => $leadId]]);
        if (!$key || !$value) {
            return $sources;
        }
        return array_filter($sources, fn($src) => data_get($src, $key) == $value);
    }

    #[ArrayShape([
        'id' => 'int',
        'name' => 'string',
        'login' => 'string',
        'role' => 'string',
        'confirmed' => 'bool',
        'allowAllDeals' => 'bool',
        'sources' => 'array',
        'metadata' => 'array',
    ])]
    public function getOperator($id): ?array
    {
        return $this->getEntity($this->getOperators(), 'id', $id);
    }

    public function getOperators(): ?array
    {
        return $this->apiClient->get('managers');
    }

    protected function getEntity(array $entities, string $nestedKey, $valueForKey): ?array
    {
        return Arr::first($entities, fn($value, $key) => data_get($entities, $key . '.' . $nestedKey) == $valueForKey);
    }

    #[ArrayShape([
        'customerId' => 'int',
        'userId' => 'int',
        'statusId' => 'int',
    ])]
    public function getLead($id): array
    {
        return $this->apiClient->get('leads/:id', ['params' => ['id' => $id]]);
    }

    #[ArrayShape([
        'id' => 'int',
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'avatar' => 'string',
        'address' => 'string',
        'login' => 'string',
        'profiles' => 'array'
    ])]
    public function getContact($id): array
    {
        return $this->apiClient->get('customers/:id', ['params' => ['id' => $id]]);
    }
}
