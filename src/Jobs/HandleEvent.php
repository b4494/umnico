<?php

namespace BmPlatform\Umnico\Jobs;

use BmPlatform\Abstraction\Enums\JobPriority;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\QueueJob;
use BmPlatform\Umnico\EventHandler;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use BmPlatform\Umnico\Utils\DataWrap;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class HandleEvent implements QueueJob
{
    use SerializesModels;

    protected Carbon $timestamp;

    public function __construct(protected readonly AppInstance $appInstance, protected array $data)
    {
        $this->timestamp = now();
    }

    public function handle(): void
    {
        try {
            (new EventHandler)(new DataWrap($this->appInstance->getHandler(), $this->data, $this->timestamp));
        }

        catch (IgnoreProcessingException $e) {
            Log::info($e->getMessage(), [ 'appInstance' => $this->appInstance, '_level' => Logger::WARNING ]);
        }

        catch (ErrorException $e) {
            Log::warning($e->getMessage(), [ 'appInstance' => $this->appInstance, 'code' => $e->errorCode->value, '_level' => Logger::WARNING ]);
        }
    }

    public function getAppInstance(): AppInstance
    {
        return $this->appInstance;
    }

    public function getPriority(): JobPriority
    {
        return match ($this->data['type']) {
            'message.incoming', 'lead.changed.status' => JobPriority::Normal,
            default => JobPriority::Low,
        };
    }
}