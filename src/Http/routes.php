<?php

use Illuminate\Support\Facades\Route;
use BmPlatform\Umnico\Http\IntegrationController;
use BmPlatform\Umnico\Http\CallbackController;

Route::namespace('BmPlatform\Umnico\Http')->name('umnico.')->group(function () {
    Route::post('integrations/umnico/webhook/{callbackId}', CallbackController::class)->name('callback');
    Route::get('integrations/umnico/{apiToken}', IntegrationController::class)->name('integration');
    Route::get('integrations/umnico', IntegrationController::class)->name('integration.welcome');
});

