<?php

namespace BmPlatform\Umnico\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\Integrator;
use BmPlatform\Umnico\SetWebhook;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class IntegrationController
{
    public function __invoke(Request $request, Hub $hub, $apiToken = null)
    {
        if (!$apiToken) {
            return view('umnico::integrate');
        }

        // Make an app data that contains all info to create user on the other side
        $makeAppData = new Integrator(
            ['api_domain' => Config::get('umnico.servers.ru.api_domain')] + [
                'token' => $apiToken,
                'user_token' => $request->input('userToken'),
            ]
        );

        /** @var \BmPlatform\Abstraction\DataTypes\Operator $user */
        [ $appData, $user ] = $makeAppData();

        // Make or update app instance from app data
        $instance = $hub->integrate($appData);

        // Redirect user to the web interface
        return new RedirectResponse($hub->webUrl($hub->getAuthToken($instance, $user), [ 'lang' => $user?->locale ]));
    }
}
