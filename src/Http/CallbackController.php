<?php

namespace BmPlatform\Umnico\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\EventHandler;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use BmPlatform\Umnico\Jobs\HandleEvent;
use BmPlatform\Umnico\Utils\DataWrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;

class CallbackController
{
    public function __invoke(Request $request, Hub $hub, $callbackId)
    {
        if (!$app = $hub->findAppByCallbackId('umnico', $callbackId)) {
            Log::warning('Failed to resolve app by callback', [ 'callbackId' => $callbackId, 'appType' => 'umnico' ]);

            return;
        }

        $data = $request->toArray();

        if (!($type = $data['type'] ?? null)) {
            Log::info('Webhook payload is incomplete.', [
                'whPayload' => $data,
                'appInstance' => $app,
                'callbackId' => $callbackId,
            ]);

            return;
        }

        if (isset(EventHandler::CLASS_MAP[$type])) {
            Log::info('WH', [ 'data' => $data, 'appInstance' => $app ]);

            Bus::dispatch(new HandleEvent($app, $data));
        }
    }
}
