<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\Utils;
use Carbon\CarbonTimeZone;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class Integrator
{
    public function __construct(public readonly array $data)
    {
        //
    }

    public function __invoke(): array
    {
        $apiClient = $this->createApiClient();
        $account = ($this->createUserApiClient() ?: $apiClient)->get('account/me');
        $managers = $apiClient->get('managers');

        $owner = collect($managers)->where('role', 'owner')->sortBy('id', SORT_NUMERIC)->first();

        if (!$account || !$owner) {
            Log::error('Crucial integration data missing', [ 'account' => $account, 'owner' => $owner, 'appType' => 'umnico' ]);

            throw new ErrorException(
                ErrorCode::IntegrationNotPossible,
                'Crucial integration data missing'
            );
        }

        $user = $account['user'] ?? null;

        return [ new AppIntegrationData(
            type: 'umnico',
            externalId: $this->serverId() . ':' . data_get($account, 'account.id'),
            name: $owner['name'] ?: $owner['login'],
            timeZone: CarbonTimeZone::createFromHourOffset($account['account']['timezone'] ?? 0),
            email: $owner['login'],
            extraData: [
                ExtraDataProps::TOKEN => $this->data['token'],
                ExtraDataProps::ADMIN_OPERATOR_ID => $owner['id'],
            ],
        ), $user ? new Operator(
            externalId: $user['id'],
            name: $user['name'],
            email: $user['login'],
            locale: mb_strtolower($user['lang']),
        ) : null ];
    }

    protected function createApiClient(): ApiClient
    {
        return new ApiClient($this->data['api_domain'], $this->data['token']);
    }

    protected function createUserApiClient(): ?ApiClient
    {
        if (!isset($this->data['user_token'])) return null;

        return new ApiClient($this->data['api_domain'], $this->data['user_token'], tokenType: 'JWT');
    }

    protected function serverId()
    {
        if ($server = Utils::matchServer($this->data['api_domain'])) {
            return $server;
        }

        throw new ErrorException(ErrorCode::IntegrationNotPossible, 'Api server is not registered');
    }
}
