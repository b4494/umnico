<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\Umnico\Utils\ExtraDataProps;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/umnico.php', 'umnico');
        $this->mergeConfigFrom(__DIR__.'/../config/umnico_params.php', 'umnico_params');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Resources/Views', 'umnico');
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'umnico');

        $config = $this->app['config'];

        $this->makeHub()->registerAppType('umnico', fn (AppInstance $app) => new AppHandler($app, $config), [
            'schema' => static fn () => $config->get('umnico'),
            'registerVariables' => $this->registerVariables(),
        ]);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }

    private function registerVariables()
    {
        return static function (VariableRegistrar $registrar) {
            $registrar->complex('lead', static function (VariableRegistrar $registrar) {
                $registrar->pk('id');
                $registrar->pk('statusId')->name('ID статуса');
                $registrar->int('amount');
                $registrar->text('address');
                $registrar->text('ttn')->name('Номер накладной');
                $registrar->mixed('customData')->array()->name('Приватные поля');
                $registrar->mixed('customFields')->array()->name('Публичные поля');
                $registrar->pk('paymentTypeId')->name('Идентификатор типа оплаты');
                $registrar->mixed('items')->name('Товары')->array();
                $registrar->datetime('createdAt');
            }, static fn (RuntimeContext $ctx) => $ctx->chat()->getExtraData()[ExtraDataProps::LEAD_DATA])
                ->name('Воронка')
                ->hideFromConditions()
                ->category(VariableCategory::chat)
            ;

            $registrar->text('contact.login', static fn (Contact $c) => $c->getExtraData()[ExtraDataProps::CONTACT_LOGIN] ?? null)
                ->name('Логин в мессенджере');

            $registrar->complex('contact.profiles', static function (VariableRegistrar $registrar) {
                $registrar->pk('id');
                $registrar->pk('socialId')->name('Внешний ID');
                $registrar->text('type')->name('Тип профиля');
                $registrar->text('profileUrl')->name('Ссылка на публичный профиль');
                $registrar->text('login')->name('Логин');
            }, static fn (Contact $c) => $c->getExtraData()[ExtraDataProps::CONTACT_PROFILES] ?? null)
                ->array()->name('Профили');
        };
    }
}
