<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\Features\HasOperatorGroups;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\Features\HasTags;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\Utils;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use RuntimeException;

class AppHandler implements AppHandlerInterface, HasOperators,
                            HasTags//, HasOperatorGroups TODO: I suppose Umnico doesn't support operator groups, I need to make sure.
{
    public function __construct(
        public readonly AppInstance $user,
        protected Repository $config,
        private ?ApiClient $apiClient = null,
        private ?Commands $commands = null,
        private ?ApiCommands $apiCommands = null,
    ) {
        //
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        // TODO: messenger instances don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect($this->getApiClient()->get('integrations'))
                ->filter(
                    fn(array $remoteMessengerInstance) => in_array(
                        $remoteMessengerInstance['type'],
                        array_keys(Config::get('umnico.messenger_types'))
                    )
                )
                ->map(fn(array $remoteMessengerInstance) => (new DataWrap($this, [
                    DataWrap::DATA_KEY => $remoteMessengerInstance,
                    DataWrap::DATA_SOURCE_KEY => DataWrap::DATA_SOURCE_API
                ]))->integration())
                ->all(), null
        );
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        // TODO: operators don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect(
                $this->getApiCommands()->getOperators()
            )->map(
                fn(array $remoteOperator) => (new DataWrap($this, [
                    DataWrap::DATA_KEY => $remoteOperator,
                    DataWrap::DATA_SOURCE_KEY => DataWrap::DATA_SOURCE_API
                ]))->manager()
            )->all(), null
        );
    }

    public function getCommands(): \BmPlatform\Abstraction\Interfaces\Commands\Commands
    {
        if (isset($this->commands)) {
            return $this->commands;
        }

        return $this->commands = new Commands($this);
    }

    public function getApiClient(): ApiClient
    {
        if (isset($this->apiClient)) {
            return $this->apiClient;
        }

        [$server,] = Utils::parseExternalId($this->user->getExternalId());

        if (!$domain = $this->config->get("umnico.servers.{$server}.api_domain")) {
            throw new RuntimeException("Umnico server [$server] is not configured.");
        }

        return $this->apiClient = new ApiClient($domain, $this->user->getExtraData()['token'], [
            'appInstance' => $this->user,
        ]);
    }

    public function getTags(mixed $cursor = null): IterableData
    {
        // TODO: tags don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect($this->getApiClient()->get('tags'))->map(
                fn(array $remoteTagInstance) => new Tag(
                    externalId: $remoteTagInstance['id'],
                    label: $remoteTagInstance['tag']
                )
            )->all(), null
        );
    }

    public function getApiCommands(): ApiCommands
    {
        if (isset($this->apiCommands)) {
            return $this->apiCommands;
        }

        return $this->apiCommands = new ApiCommands($this->getApiClient());
    }

    public function allowedTransports(): array
    {
        return array_keys($this->config->get('umnico.messenger_types'));
    }

    public function status(): AppExternalStatus
    {
        try {
            $this->getApiClient()->get('account/me');
        } catch (\Exception $e) {
            logger()->warning('GET account/me request failed => disabling app', ['appType' => 'umnico', 'appInstanceExternalId' => $this->user->getExternalId(), 'errorMessage' => $e->getMessage()]);
            return new AppExternalStatus(AppStatus::Disabled);
        }
        return new AppExternalStatus(AppStatus::Active);
    }

    public function activate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))();
    }

    public function deactivate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))(enabled: false);
    }
}
