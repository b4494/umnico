<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Exceptions\IgnoreProcessingException;
use BmPlatform\Umnico\Utils\DataWrap;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

class EventHandler
{
    const CLASS_MAP = [
        'message.incoming' => EventHandlers\MessageIncoming::class,
        'message.outgoing' => EventHandlers\MessageOutgoing::class,
        'customer.changed' => EventHandlers\CustomerChanged::class,
        'lead.changed.status' => EventHandlers\LeadChangedStatus::class,
//        'lead.changed' => EventHandlers\LeadChanged::class,
    ];

    public function __construct()
    {
        //
    }

    public function __invoke(DataWrap $data)
    {
        if (in_array($data->getDataSource(), ['lead', 'message', 'integration'])) {
            $integrationType = $data->data['message']['sa']['type'] ?? $data->data['lead']['socialAccount']['type'] ?? $data->data['integration']['type'];
            $availableTypes = array_keys(Config::get('umnico.messenger_types'));
            if (!in_array($integrationType, $availableTypes)) {
               throw new IgnoreProcessingException(ErrorCode::FeatureNotSupported, 'Webhook ' . $data->getDataSource(), 'Messenger type ' . $integrationType . ' is not supported');
            }
        }

        if (!$class = $this->getClass($data['type'] ?? null)) {
            return null;
        }

        $result = (new $class)($data);

        if (!$result instanceof \Traversable) {
            $result = [$result];
        }

        foreach ($result as $event) {
            if ($event) $data->module->user->dispatchEvent($event);
        }
    }

    private function getClass(?string $webhookType): ?string
    {
        return self::CLASS_MAP[$webhookType] ?? null;
    }
}
