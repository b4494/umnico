<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\Entities\SourceEntity;
use BmPlatform\Umnico\Utils\Utils;

trait TagCommands
{
    public function attachTagToChat(ChatTagRequest $request): void
    {
        Utils::failIfOldStructure($request->chat);
        if ($request->forTicket) {
            throw new ErrorException(ErrorCode::FeatureNotSupported, 'Umnico supports tags for chats only');
        }

        $this->module->getApiCommands()->addTagToLead(
            SourceEntity::getLeadId($request->chat),
            $request->tag->getLabel()
        );
    }

    public function detachTagFromChat(ChatTagRequest $request): void
    {
        Utils::failIfOldStructure($request->chat);
        if ($request->forTicket) {
            throw new ErrorException(ErrorCode::FeatureNotSupported, 'Umnico supports tags for chats only');
        }

        $this->module->getApiCommands()->deleteTagFromLead(
            SourceEntity::getLeadId($request->chat),
            $request->tag->getLabel()
        );
    }
}
