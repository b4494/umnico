<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\Entities\SourceEntity;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\Utils;

trait OperatorCommands
{
    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        Utils::failIfOldStructure($request->chat);
        if ($request->target instanceof OperatorGroup) {
            throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                'Umnico doesn\'t have a representation of OperatorGroup'
            );
        }

        $leadId = SourceEntity::getLeadId($request->chat);
        $operatorId = $request->target->getExternalId();

        $this->module->getApiCommands()->transferLeadToOperatorAndAcceptIfNeeded($leadId, ['userId' => (int)$operatorId]);

        return new NewOperatorResponse($operatorId);
    }
}
