<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Requests\BaseMessageRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\Entities\SourceEntity;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

use JetBrains\PhpStorm\ArrayShape;

use function with;

trait MessageCommands
{
    protected array $sources;

    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        throw new ErrorException(
            ErrorCode::FeatureNotSupported,
            'Action [sendSystemMessage] is not supported for Umnico.'
        );
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return $this->sendMessage(
            $request->chat,
            [ 'text' => $request->text ],
            $request->quickReplies,
            $request->inlineButtons,
        );
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        $media = $this->getUmnicoMedia($request);

        return $this->sendMessage($request->chat, [
            'text' => $request->caption ?: '',
            'attachment' => $media,
        ], $request->quickReplies, $request->inlineButtons);
    }

    #[ArrayShape([
        'type' => 'string',
        'media' => 'array',
    ])]
    protected function getUmnicoMedia(SendMediaRequest $request): array
    {
        $multipart = $this->formMultipartArrayForMediaUpload($request);

        return $this->module->getApiCommands()->uploadMedia($multipart);
    }

    protected function formMultipartArrayForMediaUpload(SendMediaRequest $request): array
    {
        $media = $this->formMultipartGuzzleArrayForFile($request->file);

        try {
            $sourceIdForMessaging = SourceEntity::getIdForMessageSending($request->chat);
        } catch (ErrorException $e) {
            if ($e->errorCode == ErrorCode::DataMissing) {
                $sources = $this->getSources($request->chat->getExternalId());

                $sourceIdForMessaging = Utils::resolveMessagingIdForMigratedChats($sources, $request->chat->getMessengerInstance()->getExternalType(), $request->chat->getExternalId());
            } else {
                throw $e;
            }
        }

        $source = [
            'name' => 'source',
            'contents' => $sourceIdForMessaging,
        ];

        return [$source, $media];
    }

    #[ArrayShape([
        'name' => 'string',
        'filename' => 'string',
        'Mime-Type' => 'null|string',
        'contents' => 'string'
    ])]
    protected function formMultipartGuzzleArrayForFile(MediaFile $file): array
    {
        try {
            $content = file_get_contents($file->url);
        } catch (\Exception $e) {
            Log::error(
                'Could not get contents from url ' . $file->url . ' with file_get_contents',
                ['appInstance' => $this->module->user]
            );

            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not fetch file contents from provided URL');
        }

        if (!$content) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not fetch file contents from provided URL');
        }

        $mime = $file->mime ?? ($this->getFileMime($content) ?: null);

        return array_filter([
            'name' => 'media',
            'filename' => $file->name ?: $this->generateFileName($file->type, $mime),
            'Mime-Type' => $mime,
            'contents' => $content,
        ]);
    }

    protected function generateFileName(MediaFileType $type, null|string $mime): string
    {
        if ($mime) {
            return $type->name . '.' . explode('/', $mime)[1];
        }
        return $type->name;
    }

    protected function getFileMime(string $content): false|string
    {
        $finfo = finfo_open(flags: FILEINFO_MIME_TYPE);
        $mime = finfo_buffer(finfo: $finfo, string: $content, flags: FILEINFO_MIME_TYPE);
        finfo_close($finfo);
        return $mime;
    }

    /** @param array $inlineButtons * @param array $quickReplies*@throws ErrorException */
    protected function sendMessage(Chat $chat, array $payload, array $quickReplies, array $inlineButtons): MessageSendResult
    {
        Utils::failIfOldStructure($chat);
        $leadId = SourceEntity::getLeadId($chat);
        $messengerType = $chat->getMessengerInstance()->getExternalType();
        try {
            $sourceIdForMessaging = SourceEntity::getIdForMessageSending($chat);
        } catch (ErrorException $e) {
            if ($e->errorCode == ErrorCode::DataMissing) {
                $sources = $this->getSources(
                    Utils::parseExternalId($chat->getExternalId())[0]
                );
                $sourceIdForMessaging = Utils::resolveMessagingIdForMigratedChats($sources, $messengerType, $chat->getExternalId());
            } else {
                throw $e;
            }
        }

        $chatNeedsToBeLatest = in_array($messengerType, DataWrap::USE_LATEST_SOURCE);
        if ($chatNeedsToBeLatest && !$this->isLatest($leadId, $sourceIdForMessaging)) {
            throw new ErrorException(ErrorCode::ChatNotFound, 'Trying to send text to an old chat!');
        }

        $operatorId = $this->module->user->getExtraData()[ExtraDataProps::ADMIN_OPERATOR_ID];
        return with(
            $this->module->getApiCommands()->sendMessage(
                leadId: $leadId,
                sourceRealId: $sourceIdForMessaging,
                operatorId: $operatorId,
                payload: $payload,
                replyMarkup: $this->makeReplyMarkup($messengerType, $quickReplies, $inlineButtons)
            ),
            fn($resp) => new MessageSendResult($resp[0]['messageId'], data_get($resp, '0.source.id'))
        );
    }

    protected function isLatest($leadId, $sourceRealId): bool
    {
        $sources = array_filter($this->getSources($leadId), fn($src) => isset($src['realId']));
        foreach ($sources as $source) {
            if ($source['realId'] > $sourceRealId) {
                return false;
            }
        }
        return true;
    }

    protected function getSources($leadId): array
    {
        if (!isset($this->sources)) {
            $this->sources = $this->module->getApiCommands()->getSources($leadId);
        }
        return $this->sources;
    }
}
