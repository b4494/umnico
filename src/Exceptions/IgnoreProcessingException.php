<?php

namespace BmPlatform\Umnico\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;

class IgnoreProcessingException extends \BmPlatform\Abstraction\Exceptions\ErrorException
{
    public function __construct(ErrorCode $errorCode, string $wasIgnored, string $reason, string $message = '')
    {
        $message = $wasIgnored . ' was ignored. REASON: ' . $reason . PHP_EOL . $message;
        parent::__construct($errorCode, $message);
    }
}
