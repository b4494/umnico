<?php return [
    "contact_email" => "Email",
    "contact_name" => "Name",
    "contact_address" => "Address",
    "contact_phone" => "Phone",
    "lead_status_changed" => 'Lead status changed',
];