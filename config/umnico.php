<?php

return [
    'servers' => [
        'ru' => [
            'api_domain' => 'https://api.umnico.com',
            'front_domain' => 'https://umnico.com',
            'matcher' => 'https://(api)\.umnico\.com',
            'locale' => 'ru',
            // ???
            'currency' => 'RUB',
        ],
    ],

    'title' => 'Umnico',

    'features' => [
        'operators' => [
            'read' => true,
            'transfer' => true,
        ],

        'tags' => [
            'read' => true,
            'attach' => true,
            'detach' => true,
            'attachForTicket' => false,
            'detachForTicket' => false,
        ],
    ],

    'events' => [
        'inboxFlags' => [
            'newChatCreated' => true,
            'newTicketOpened' => false,
            'externalPostComment' => true,
        ],
        'outboxSent' => true,
        'newContactChatAdded' => false,
        'chatTicketClosed' => false,
        'chatTransferred' => false,
        'chatTagAttached' => false,
        'chatTagDetached' => false,
        'chatTicketTagAttached' => false,
        'chatTicketTagDetached' => false,
        'messageStatusChanged' => false,
        'contactDataUpdated' => true,
        'externalItem' => true,
    ],

    'data' => [
        'integration' => [
            'timezone' => false,
            'users' => true,
        ],

        'user' => [
            'locale' => true,
        ],

        'contact' => [
            'phone' => true,
            'email' => true,
            'avatar' => true,
            'messengerId' => false,
        ],

        'chat' => [
            'messengerId' => true,
            'operator' => false,
        ],

        'operator' => [
            'phone' => false,
            'email' => true,
            'avatar' => false,
        ],

        'messengerInstance' => [
            'messengerId' => false,
        ],

        'externalItem' => [
            'url' => true,
        ],
    ],

    'actions' => [
        'update_contact_data' => [
            'supported' => true,

            'editableFields' => [
                'type' => 'object',
                'properties' => [
                    'name' => [
                        'type' => 'string',
                        'title' => 'umnico::app.contact_name',
                    ],

                    'phone' => [
                        'type' => 'phone',
                        'title' => 'umnico::app.contact_phone',
                    ],

                    'email' => [
                        'type' => 'text',
                        'title' => 'umnico::app.contact_email',
                    ],

                    'address' => [
                        'type' => 'string',
                        'title' => 'umnico::app.contact_address',
                    ],
                ],
            ],
        ]
    ],

    'custom_events' => [
        [ 'id' => 'leadStatusChanged', 'title' => 'umnico::app.lead_status_changed', 'icon' => 'funnel' ],
    ],

    'messenger_types' => [
        'default' => [
            'name' => 'Unknown messenger',

            'text' => [
                // ??? Umnico doesn't seem to have message length specification
                'max_length' => 4096,
            ],

            'image' => [
                'enabled' => true,
                'mime' => ['image/jpeg', 'image/png', 'image/gif'],
                'max_file_size' => 5 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'video' => [
                'enabled' => true,
                'mime' => ['video/mp4', 'video/3gpp'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'document' => [
                'enabled' => true,
                'mime' => ['*'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'file_name' => true,
                'caption_max_length' => 4096,
            ],

            'audio' => [
                'enabled' => true,
                'mime' => ['audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/aac'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 1000,
            ],
        ],

//      Webhooks are coming in with unexpected payloads
//        'fb_group' => [
//            'extends' => 'default',
//            'name' => 'Facebook (group)',
//        ],

        'fb_messenger' => [
            'extends' => 'default',
            'internal_type' => 'facebook',
            'name' => 'Facebook (messenger)',
        ],

        // exists, but is not described in docs
        'discord' => [
            'extends' => 'default',
            'name' => 'Discord'
        ],

//      Webhooks are coming in with unexpected payloads
//        'vk_group' => [
//            'extends' => 'default',
//            'internal_type' => 'vk',
//            'name' => 'Группа Вконтакте',
//        ],

        'telebot' => [
            'extends' => 'default',
            'name' => 'Telegram',
            'internal_type' => 'telegram',

            'inline_buttons' => [
                'text' => true,
                'text_max_length' => 20,
                'url' => true,
                'payload' => true,
                'payload_max_length' => 64,
                'max_count' => 20,
            ],

            'quick_replies' => [
                'text' => true,
                'text_max_length' => 20,
//                'phone' => true,
//                'geolocation' => true,
                'max_count' => 20,
            ],
        ],

        'telegram' => [
            'extends' => 'default',
            'name' => 'Telegram User'
        ],

        'whatsapp2' => [
            'extends' => 'default',
            'name' => 'Whatsapp',
            'internal_type' => 'whatsapp',
        ],

        'waba' => [
            'extends' => 'default',
            'name' => 'WABA',
            'internal_type' => 'whatsapp',
        ],

        'waba_cloud' => [
            'extends' => 'default',
            'internal_type' => 'whatsapp',
            'name' => 'WABA',
        ],

        'widget' => [
            'extends' => 'default',
            'name' => 'External channel',
        ],

        'instagramV3' => [
            'extends' => 'default',
            'name' => 'Instagram',
            'internal_type' => 'instagram',
            'video' => [
                'enabled' => false,
            ],
            'document' => [
                'enabled' => false,
            ],
            'audio' => [
                'enabled' => false,
            ],
        ],

        'viber_bot' => [
            'extends' => 'default',
            'name' => 'Viber',
            'internal_type' => 'viber_public'
        ],

        'ok' => [
            'extends' => 'default',
            'name' => 'Одноклассники',
        ],

        'avito' => [
            'extends' => 'default',
            'name' => 'Avito',
        ],

        'mailbox' => [
            'extends' => 'default',
            'name' => 'Mailbox',
        ],
    ]
];

