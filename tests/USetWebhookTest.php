<?php

use Mockery as m;

class USetWebhookTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testSetsWebhook()
    {
        \Illuminate\Support\Facades\App::expects('isLocal')->andReturn(false)->zeroOrMoreTimes();

        \Illuminate\Support\Facades\URL::expects('route')->with('umnico.callback', ['callbackId' => 'hash'])->andReturn(
            'url/hash'
        )->zeroOrMoreTimes();

        $handler = m::mock(\BmPlatform\Umnico\AppHandler::class, [
            $user = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $handler->expects('getApiClient')->andReturn(
            $apiClient = m::mock(\BmPlatform\Umnico\ApiClient::class)
        )->zeroOrMoreTimes();

        $hub = m::mock(\BmPlatform\Abstraction\Interfaces\Hub::class);
        $hub->expects('getCallbackId')->with($user)->andReturn('hash')->zeroOrMoreTimes();

        $target = new \BmPlatform\Umnico\SetWebhook($handler, $hub);

        $apiClient->expects('get')->with('webhooks')->andReturn([]);
        $apiClient->expects('post')->with('webhooks', [
            'json' => [
                'url' => 'url/hash',
            ],
        ])->zeroOrMoreTimes();

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            ['id' => 1, 'url' => 'url/hash'],
        ]);

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            ['id' => 1, 'url' => 'url/hash'],
        ]);

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            ['id' => 1, 'url' => 'url/hash'],
        ]);
        $apiClient->expects('delete')->with('webhooks/:id', [
            'params' => [
                'id' => 1
            ]
        ]);

        $target(false);
    }
}
