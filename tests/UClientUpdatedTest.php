<?php

use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use Mockery as m;

class UClientUpdatedTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testClientUpdatedEvent()
    {
        $webhookDataJson = '{
    "accountId": 1607,
    "customerId": 21353130,
    "type": "customer.changed",
    "customer": {
        "id": 21353130,
        "login": "Bogdan",
        "name": "Bogdan",
        "avatar": "https://media-direct.cdn.viber.com/download_photo?dlid=Y3lZYZrnJ2Dl5LxPLnyEt5iDL6CzFQd2nRt9M60b9tMzgNCqV_3kgpL2PppnJcazy8iq9yjvpDCqS_x3QyqXLSVOG8Fwq80LRcL69STcIhyocz0hmMIovOJyhT3-MnNuAihtXA&fltp=jpg&imsz=0000",
        "email": "newemail@september.ninth",
        "phone": "8 999 999 99 99",
        "address": null,
        "profiles": [
            {
                "id": 20274389,
                "login": "Bogdan",
                "type": "viber_bot",
                "socialId": "/mXOBOxeQLIaKhvGq6/+gA==",
                "profileUrl": null
            }
        ]
    }
}';

        $appHandler = m::mock(\BmPlatform\Umnico\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $data = m::mock(\BmPlatform\Umnico\Utils\DataWrap::class, [$appHandler, json_decode($webhookDataJson, true)]);
        $data->expects('timestamp')->andReturn($ts = Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new \BmPlatform\Umnico\EventHandlers\CustomerChanged();
        $this->assertEquals(
            new \BmPlatform\Abstraction\Events\ContactDataUpdated(
                contact: new \BmPlatform\Abstraction\DataTypes\Contact(
                    externalId: 21353130,
                    name: "Bogdan",
                    phone: "8 999 999 99 99",
                    email: "newemail@september.ninth",
                    avatarUrl: "https://media-direct.cdn.viber.com/download_photo?dlid=Y3lZYZrnJ2Dl5LxPLnyEt5iDL6CzFQd2nRt9M60b9tMzgNCqV_3kgpL2PppnJcazy8iq9yjvpDCqS_x3QyqXLSVOG8Fwq80LRcL69STcIhyocz0hmMIovOJyhT3-MnNuAihtXA&fltp=jpg&imsz=0000",
                    extraFields: ['address' => null],
                    extraData: Helpers::withoutEmptyStrings([
                        ExtraDataProps::CONTACT_LOGIN => 'Bogdan',
                        ExtraDataProps::CONTACT_PROFILES => [
                            [
                                "id" => 20274389,
                                "login" => "Bogdan",
                                "type" => "viber_bot",
                                "socialId" => "/mXOBOxeQLIaKhvGq6/+gA==",
                                "profileUrl" => null,
                            ]
                        ]
                    ])
                ),
                timestamp: $ts,
            ),
            $eventHandler($data)
        );
    }
}
