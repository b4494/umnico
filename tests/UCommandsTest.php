<?php


use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Umnico\ApiClient;
use BmPlatform\Abstraction\Interfaces\Operator;
use \BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Commands;
use BmPlatform\Umnico\AppHandler;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class UCommandsTest extends MockeryTestCase
{
    const LEAD_ID = 'leadid';

    public function testSendsAvailableMessageTypes()
    {
        $textMessageSentSuccess = json_decode(
            '[
    {
        "customId": "UmnicoCustomId8818238712731839",
        "messageId": 5750682273333003000,
        "datetime": 1663127272758,
        "sa": {
            "id": 42202,
            "type": "viber_bot",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000"
        },
        "message": {
            "text": "text"
        },
        "incoming": false,
        "sender": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000",
            "type": "viber_bot",
            "socialId": null
        },
        "source": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "realId": 27330780,
            "name": null,
            "type": "message",
            "saId": 42202,
            "sender": "/mXOBOxeQLIaKhvGq6/+gA==",
            "token": null,
            "identifier": "4fbce4e56027e79a-24479e9416db5de8-bd28f35b93ae4b74",
            "expires": null
        }
    }
]',
            true
        );
        $ownerId = '11';
        $virtualSourceRealId = '27330780';
        $leadId = self::LEAD_ID;
        $customId = 'UmnicoCustomId8818238712731839';
        $textPayload = ['text' => 'text'];
        $additionalPayload = [
            'source' => $virtualSourceRealId,
            'userId' => $ownerId,
            'customId' => $customId
        ];

        $messenger = Mockery::mock(MessengerInstance::class);
        $messenger->expects('getExternalType')->andReturn('viber_bot')->times(2);
        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExtraData')->andReturn([ExtraDataProps::LEAD_DATA => [], ExtraDataProps::SOURCE_REAL_ID => $virtualSourceRealId])->times(3);
        $chat->expects('getMessengerInstance')->andReturn($messenger)->times(2);
        $chat->expects('getExternalId')->andReturn($leadId)->times(1);

        \Illuminate\Support\Facades\Config::shouldReceive('get')->with('umnico_params.custom_id')->andReturn(
            $customId
        )->times(1);

        $apiClient = Mockery::mock(\BmPlatform\Umnico\ApiClient::class);
        $apiClient->expects('post')->with('messaging/:lead-id/send', [
            'params' => ['lead-id' => $leadId],
            'json' => ['message' => $textPayload] + $additionalPayload
        ])->andReturn($textMessageSentSuccess);
        $apiCommands = new \BmPlatform\Umnico\ApiCommands($apiClient);

        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $appInstance->expects('getExtraData')->andReturn([ExtraDataProps::ADMIN_OPERATOR_ID => $ownerId])->times(1);
        $module = new AppHandler(
            $appInstance,
            Mockery::mock(Illuminate\Contracts\Config\Repository::class),
            apiCommands: $apiCommands
        );

        $commands = new Commands($module);
        $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'text'));
        $this->assertEquals(new MessageSendResult(5750682273333003000, "/mXOBOxeQLIaKhvGq6/+gA=="), $resp);

        $chat->expects('getExtraData')->andReturn([
            ExtraDataProps::LEAD_ID => $leadId,
        ])->times(1);

        try {
            $commands->sendTextMessage(new SendTextMessageRequest($chat, 'text'));
            $this->fail('expected ErrorException');
        } catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::ChatNotFound, $e->errorCode);
        }

//        \Illuminate\Support\Facades\Log::shouldReceive('error')->with(
//            'System messages are not supported for Umnico.', [ 'appInstance' => $commands->module->user ]
//        )->times(1);
        try {
            $commands->sendSystemMessage(new SendSystemMessageRequest($chat, 'text'));
            $this->fail('expected ErrorException');
        } catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::FeatureNotSupported, $e->errorCode);
        }

        // Testing media message fails when file_get_contents fails
        $nonexistentPath = __DIR__ . '/FileGetContents/image.jpg';
        \Illuminate\Support\Facades\Log::shouldReceive('error')->with(
            'Could not get contents from url ' . $nonexistentPath . ' with file_get_contents', [ 'appInstance' => $commands->module->user ]
        )->times(1);
        try {
            $commands->sendMediaMessage(
                new SendMediaRequest($chat, new MediaFile(MediaFileType::Image, $nonexistentPath))
            );
            $this->fail('Expected Exception');
        } catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::InvalidResponseData, $e->errorCode);
        }
    }

    public function testFieldsUpdated()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $contact = Mockery::mock(Contact::class);
        $contact->expects('getExternalId')->andReturn(1)->times(2);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiClient')->andReturn($apiClient)->times(2);

        $commands = new Commands($module);

        $apiClient->expects('put')->with('customers/:customerId', [
            'params' => ['customerId' => 1],
            'json' => [
                'name' => 'name',
                'phone' => 'phone',
                'email' => 'comment',
            ],
        ])->once();

        $commands->updateContactData(
            new UpdateContactDataRequest(
                $contact,
                name: 'name', phone: 'phone', email: 'comment'
            )
        );

        $apiClient->expects('put')->with('customers/:customerId', [
            'params' => ['customerId' => 1],
            'json' => [
                'address' => 'address',
            ],
        ])->once();

        $commands->updateContactData(
            new UpdateContactDataRequest(
                $contact,
                fields: ['address' => 'address']
            )
        );

        // empty payload
        $commands->updateContactData(
            new UpdateContactDataRequest(
                $contact
            )
        );
    }

    public function testTransfersToOperator()
    {
        $leadId = self::LEAD_ID;
        $operatorId = 21;

        $chat = $this->mockChat();

        $operator = Mockery::mock(\BmPlatform\Abstraction\Interfaces\Operator::class);
        $operator->expects('getExternalId')->andReturn($operatorId)->times(1);

        $chat->expects('getExtraData')->andReturn([
            ExtraDataProps::LEAD_DATA => [],
        ])->times(4);

        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('put')->with(
            'leads/:id/accept',
            ['params' => ['id' => $leadId], 'json' => ['userId' => $operatorId]]
        );
        $apiCommands = Mockery::mock(new \BmPlatform\Umnico\ApiCommands($apiClient));
        $apiCommands->makePartial();

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiCommands')->andReturn($apiCommands);

        $req = new TransferChatToOperatorRequest($chat, $operator);
        $resp = new NewOperatorResponse((string)$operatorId);
        $commands = new Commands($module);
        $this->assertEquals($commands->transferChatToOperator($req), $resp);

        $operatorGroup = Mockery::mock(\BmPlatform\Abstraction\Interfaces\OperatorGroup::class);
        $req = new TransferChatToOperatorRequest($chat, $operatorGroup);
        try {
            $commands->transferChatToOperator($req);
            $this->fail('Expected error exception');
        } catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::FeatureNotSupported, $e->errorCode);
        }
    }

//    public function testChatTicketClosed()
//    {
//        $statuses = json_decode(
//            '[
//    {
//        "id": 10243,
//        "type": "active",
//        "name": "Первичный контакт",
//        "order": 1,
//        "color": "#64e4a3"
//    },
//    {
//        "id": 10244,
//        "type": "active",
//        "name": "Вторичный контакт",
//        "order": 5,
//        "color": "#ff995c"
//    },
//    {
//        "id": 10245,
//        "type": "active",
//        "name": "Принимает решение",
//        "order": 10,
//        "color": "#db51e6"
//    },
//    {
//        "id": 10246,
//        "type": "completed",
//        "name": "Успешно завершено",
//        "order": 50,
//        "color": "#5982fa"
//    },
//    {
//        "id": 10247,
//        "type": "failed",
//        "name": "Не успешно завершено",
//        "order": 55,
//        "color": "#ff5b60"
//    },
//    {
//        "id": 10248,
//        "type": "ignore",
//        "name": "Спам",
//        "order": 70,
//        "color": "#ff5b60"
//    }
//]',
//            true
//        );
//        $lead = json_decode(
//            '{
//    "id": "' . self::LEAD_ID . '",
//    "userId": 48246,
//    "statusId": 10243,
//    "read": true,
//    "amount": 0,
//    "details": null,
//    "responseTime": 1309210,
//    "customerId": 21353130,
//    "timestamp": "2022-09-14T06:57:42.771Z",
//    "address": null,
//    "ttn": null,
//    "customData": null,
//    "customFields": null,
//    "items": null
//}',
//            true
//        );
//        $leadId = self::LEAD_ID;
//        $userId = 48246;
//        $completedStatusId = 10246;
//        // not active
//        $apiClient = Mockery::mock(ApiClient::class);
//        $apiClient->expects('get')->with('leads/:id', ['params' => ['id' => $leadId]])->andReturn(
//            $lead
//        )->once();
//        $apiClient->expects('get')->with('statuses')->andReturn($statuses)->times(2);
//        $apiClient->expects('put')->with('leads/:id', [
//            'params' => ['id' => $leadId],
//            'json' => ['statusId' => $completedStatusId, 'userId' => $userId],
//        ])->andReturn(true)->once();
//
//        $module = Mockery::mock(AppHandler::class, [
//            $user = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
//            Mockery::mock(\Illuminate\Contracts\Config\Repository::class),
//        ]);
//        $apiCommands = new \BmPlatform\Umnico\ApiCommands($apiClient);
//
//        $module->expects('getApiCommands')->andReturn($apiCommands)->times(7);
//        $module->expects('getApiClient')->andReturn($apiClient)->once();
//
//        \Illuminate\Support\Facades\Event::expects('dispatch')->once();
//
//        $chat = $this->mockChat();
//        $chat->expects('getExtraData')->andReturn([ExtraDataProps::LEAD_DATA => []])->times(2);
////        $chat->expects('getExternalId')->andReturn($leadId . ':')->once();
//        $commands = new BmPlatform\Umnico\Commands($module);
//        $commands->closeChatTicket($chat);
//
//        $lead = json_decode(
//            '{
//    "id": "' . self::LEAD_ID . '",
//    "userId": 48246,
//    "statusId": 10246,
//    "read": true,
//    "amount": 0,
//    "details": null,
//    "responseTime": 1309210,
//    "customerId": 21353130,
//    "timestamp": "2022-09-14T06:57:42.771Z",
//    "address": null,
//    "ttn": null,
//    "customData": null,
//    "customFields": null,
//    "items": null
//}',
//            true
//        );
//        $apiClient->expects('get')->with('leads/:id', ['params' => ['id' => $leadId]])->andReturn(
//            $lead
//        )->once();
//        $chat->expects('getExtraData')->andReturn([ExtraDataProps::LEAD_DATA => []])->times(2);
//        try {
//            $commands->closeChatTicket($chat);
//            $this->fail('Expected error exception');
//        } catch (ErrorException $e) {
//            $this->assertEquals(ErrorCode::ChatTicketAlreadyClosed, $e->errorCode);
//        }
//    }

    /**
     * @return \BmPlatform\Abstraction\Interfaces\Chat|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    protected function mockChat(): \Mockery\LegacyMockInterface|Chat|\Mockery\MockInterface
    {
        $mi = Mockery::mock(MessengerInstance::class);
        $mi->expects('getExternalId')->andReturn('1:telegram')->zeroOrMoreTimes();

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn(self::LEAD_ID . ':')->zeroOrMoreTimes();
        $chat->expects('getMessengerInstance')->andReturn($mi)->zeroOrMoreTimes();

        return $chat;
    }

    /**
     * // * @return array
     * // */
    protected function mockModule(): array
    {
        $apiCommands = Mockery::mock(\BmPlatform\Umnico\ApiCommands::class);

        $apiClient = Mockery::mock(ApiClient::class);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        return array($apiCommands, $apiClient, $module);
    }

    public function testReplyMarkup()
    {
        $module = Mockery::mock(AppHandler::class);
        $commands = new BmPlatform\Umnico\Commands($module);

        $this->assertEquals([
            'inline_keyboard' => [
                [ [ 'text' => 'inline button', 'callback_data' => 'payload' ] ],
                [ [ 'text' => 'url button', 'url' => 'url' ] ],
            ],
        ], $commands->makeReplyMarkup('telebot', [], [
            [ new \BmPlatform\Abstraction\DataTypes\InlineButton('inline button', 'payload') ],
            [ new \BmPlatform\Abstraction\DataTypes\InlineUrlButton('url button', 'url', 'payload') ],
        ]));

        $this->assertEquals([
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'keyboard' => [ [
                [ 'text' => 'text' ],
                [ 'text' => 'contact', 'request_contact' => true ],
                [ 'text' => 'location', 'request_location' => true ]
                            ] ]
        ], $commands->makeReplyMarkup('telebot', [[
            new \BmPlatform\Abstraction\DataTypes\QuickReply('text'),
            new \BmPlatform\Abstraction\DataTypes\QuickReply('contact', \BmPlatform\Abstraction\Enums\QuickReplyType::Phone),
            new \BmPlatform\Abstraction\DataTypes\QuickReply('location', \BmPlatform\Abstraction\Enums\QuickReplyType::Location)
                                        ]], []));
    }
}
