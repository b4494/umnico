<?php

use Mockery as m;

class UIntegratorTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testIntegrates()
    {
        $managers = '[
    {
        "id": 1886,
        "login": "itinvrn@gmail.com",
        "name": "олег",
        "role": "owner",
        "confirmed": true,
        "allowAllDeals": false,
        "allowedUsers": null,
        "sources": [],
        "metadata": {}
    },
    {
        "id": 17523,
        "login": "info@bot-marketing.com",
        "name": null,
        "role": "manager",
        "confirmed": true,
        "allowAllDeals": true,
        "allowedUsers": null,
        "sources": [],
        "metadata": {}
    }
]';
        $managersWithoutOwner = '[
    {
        "id": 17523,
        "login": "info@bot-marketing.com",
        "name": null,
        "role": "manager",
        "confirmed": true,
        "allowAllDeals": true,
        "allowedUsers": null,
        "sources": [],
        "metadata": {}
    }
]';
        $account = '{
    "account": {
        "id": 1607,
        "status": "active",
        "uiCurrency": "RUB",
        "timezone": 3
    },
    "user": {
        "id": 15,
        "login": "aaa@bbb.com",
        "name": "User",
        "role": "owner",
        "lang": "RU",
        "metadata": {
            "source": "amocrm",
            "externalId": "123213213213",
            "amocrm": {
                "email": "email@example.com"
            }
        }
    }
}';
        \Illuminate\Support\Facades\Config::expects('get')->with('umnico.servers')->andReturn([
            'ru' => ['matcher' => 'https://(api)\.umnico\.com'],
        ]);

        $integrator = m::mock(\BmPlatform\Umnico\Integrator::class, [
            [
                'token' => 'token',
                'api_domain' => 'https://api.umnico.com',
                'user_token' => 'user_token',
            ]
        ]);

        $integrator->shouldAllowMockingProtectedMethods();
        $integrator->expects('createApiClient')->andReturn(
            $apiClient = m::mock(\BmPlatform\umnico\ApiClient::class)
        )->zeroOrMoreTimes();
        $integrator->expects('createUserApiClient')->andReturn($userApiClient = m::mock(\BmPlatform\Umnico\ApiClient::class))->zeroOrMoreTimes();

        $integrator->makePartial();

        $userApiClient->expects('get')->with('account/me')->andReturn(
            json_decode(
                $account,
                true
            )
        )->once();
        $apiClient->expects('get')->with('managers')->andReturn(
            json_decode(
                $managers,
                true
            )
        )->once();

        $this->assertEquals(
            [new \BmPlatform\Abstraction\DataTypes\AppIntegrationData(
                type: 'umnico',
                externalId: 'ru:1607',
                name: 'олег',
                timeZone: \Carbon\CarbonTimeZone::createFromHourOffset(3),
                email: 'itinvrn@gmail.com',
                extraData: [
                    'token' => 'token',
                    'adminId' => 1886
                ],
            ), new \BmPlatform\Abstraction\DataTypes\Operator(
                externalId: 15,
                name: "User",
                email: "aaa@bbb.com",
                locale: "ru",
            )],
            $integrator()
        );

        \Illuminate\Support\Facades\Log::shouldReceive('error')->with('Crucial integration data missing', [
            'account' => null,
            'owner' => json_decode($managers, true)[0],
            'appType' => 'umnico'
        ])->once();
        $apiClient->expects('get')->with('managers')->andReturn(
            json_decode(
                $managers,
                true
            )
        )->once();
        $userApiClient->expects('get')->with('account/me')->andReturn(null)->once();
        try {
            $integrator();
            $this->fail('Should throw exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::IntegrationNotPossible, $e->errorCode);
        }

        \Illuminate\Support\Facades\Log::shouldReceive('error')->with('Crucial integration data missing', [
            'account' => json_decode($account, true),
            'owner' => [],
            'appType' => 'umnico'
        ]);
        $apiClient->expects('get')->with('managers')->andReturn(
            json_decode($managersWithoutOwner, true)
        );

        $userApiClient->expects('get')->with('account/me')->andReturn(
            json_decode($account, true)
        );
        try {
            $integrator();
            $this->fail('Should throw exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::IntegrationNotPossible, $e->errorCode);
        }
    }
}
