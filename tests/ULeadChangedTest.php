<?php

use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use Mockery as m;

class ULeadChangedTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testLeadChangedManagerMissing()
    {
        $webhookDataJson = '{
    "accountId": 1607,
    "leadId": 21964777,
    "type": "lead.changed",
    "lead": {
        "id": 21964777,
        "userId": null,
        "statusId": 10243,
        "read": false,
        "amount": 0,
        "details": null,
        "tags": [],
        "socialAccount": {
            "id": 42491,
            "login": "idontlikepizza",
            "type": "telebot"
        },
        "responseTime": null,
        "customer": {
            "id": 22461230,
            "login": "Maxim Troshin",
            "name": "Maxim Troshin",
            "avatar": "https://umnico.com/api/files/download/19367.jpg?sa=42491&fileId=AgACAgIAAxUAAWNGV2lMx9CTywlUv5MZBnqifJg5AAJCqTEbp6QmAAFAbzSLF368oQEAAwIAA2IAAyoE",
            "email": null,
            "phone": null
        },
        "createdAt": "2022-10-12T05:58:01.980Z",
        "message": {
            "unread": 2,
            "timestamp": "2022-10-12T05:58:07.000Z",
            "incoming": true,
            "data": {
                "text": "это я"
            }
        },
        "address": null,
        "ttn": null,
        "customData": null,
        "customFields": null,
        "paymentTypeId": null,
        "items": null
    }
}';
        $leadData = '{
        "id": 21964777,
        "userId": null,
        "statusId": 10243,
        "read": false,
        "amount": 0,
        "details": null,
        "tags": [],
        "responseTime": null,
        "createdAt": "2022-10-12T05:58:01.980Z",
        "address": null,
        "ttn": null,
        "customData": null,
        "customFields": null,
        "paymentTypeId": null,
        "items": null
    }';
        $getSourceResponse = '[{"id":"2532519","realId":28487139,"name":null,"type":"message","saId":42491,"sender":"2532519","token":null,"identifier":"5722428780"}]';

        $appHandler = m::mock(\BmPlatform\Umnico\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $apiCommands = m::mock(\BmPlatform\Umnico\ApiCommands::class);
        $apiCommands->makePartial();
        $apiCommands->expects('getSources')->with(21964777)->andReturn(
            json_decode($getSourceResponse, true)
        )->once();
        $appHandler->expects('getApiCommands')->andReturn($apiCommands)->once();
        $data = m::mock(\BmPlatform\Umnico\Utils\DataWrap::class, [$appHandler, json_decode($webhookDataJson, true)]);
        $data->shouldAllowMockingProtectedMethods();
//        $data->expects('retrieveFromApi')->with('getLead', [21964777])->andReturn(['userId' => null])->once();
        $data->expects('timestamp')->andReturn($ts = Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new \BmPlatform\Umnico\EventHandlers\LeadChanged();

        $this->assertEquals(
            new ChatDataChanged(
                new \BmPlatform\Abstraction\DataTypes\Chat(
                    externalId: '21964777',
                    messengerInstance: 42491,
                    contact: 22461230,
                    operator: null,
                    messengerId: 2532519,
                    extraData: \BmPlatform\Support\Helpers::withoutEmptyStrings([
                        ExtraDataProps::LEAD_DATA => json_decode($leadData, true),
                        ExtraDataProps::SOURCE_REAL_ID => 28487139
                    ])
                ),
                $ts,
            ),
            $eventHandler($data)
        );
    }
}
