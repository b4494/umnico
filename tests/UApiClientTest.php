<?php

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class UApiClientTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    protected function initClient(array $responses)
    {
        $handler = new \GuzzleHttp\Handler\MockHandler($responses);

        return new \BmPlatform\Umnico\ApiClient('https://domain.com', 'token', [
            'handler' => new \GuzzleHttp\HandlerStack($handler),
        ]);
    }

    public function testRequests()
    {
        $apiClient = $this->initClient([
            function (Request $request) {
                if ((string)$request->getUri() == 'https://domain.com/v1.3/api_info'
                    && $request->getHeader('Authorization')[0] == 'bearer token'
                ) {
                    return new Response(200, [], '');
                }

                return new Response(400, [], '');
            },
            new Response(200, [], json_encode([['id' => 1, 'login' => 'firstmanager@example.com'], ['id' => 2, 'login' => 'secondmanager@example.com']])),
            new Response(400, [], json_encode(['errors' => ['This user already exists']])),
            new Response(401, [], json_encode(['errors' => ['Unauthorized']])),
            new Response(403, [], null),
            new Response(404, [], json_encode(['errors' => ['Endpoint not found']])),
            new Response(422, [], json_encode([
                "errors" => [
                    "body.message should have required property 'text'"
                ]
            ])),
        ]);

        $this->assertEquals(true, $apiClient->request('GET', 'api_info'));
        $this->assertEquals(
            [['id' => 1, 'login' => 'firstmanager@example.com'], ['id' => 2, 'login' => 'secondmanager@example.com']],
            $apiClient->request('GET', '')
        );

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::BadRequest, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::AuthenticationFailed, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::AuthenticationFailed, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::NotFound, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected validation exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ValidationException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::BadRequest, $e->errorCode);
            $this->assertEquals("body.message should have required property 'text'", $e->errors->first());
        }
    }

    // TODO
    public function testAll()
    {
        $this->assertEquals(true, true);
    }
}
