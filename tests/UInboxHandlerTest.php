<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use Carbon\Carbon;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Umnico\EventHandlers\MessageIncoming;
use Mockery as m;

class UInboxHandlerTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    const MANAGER_ID = 'managerid';
    const CONTACT_ID = 'contactid';
    const SOURCE_IDENTIFIER = 'identifierid';
    const SOURCE_ID = 'sourceid';
    const LEAD_ID = 'leadid';
    const REAL_ID = 27330686;
    const SOURCE_EXTERNAL_ID = self::LEAD_ID;
    const MESSAGE_ID = 'messageid';
    const MESSAGE_EXTERNAL_ID = self::LEAD_ID . ':' . self::MESSAGE_ID;
    const INTEGRATION_ID = 'integrationid';

    /** @dataProvider dataProvider */
    public function testInboxEvents($data, $event)
    {
        $appHandler = m::mock(\BmPlatform\Umnico\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $apiCommands = m::mock(new \BmPlatform\Umnico\ApiCommands(new \BmPlatform\Umnico\ApiClient('domain', 'token')));
        $apiCommands->expects('getContact')->with(self::CONTACT_ID)->andReturn(
            json_decode(
                '{
    "id": "' . self::CONTACT_ID . '",
    "login": "Bogdan Shipilov",
    "name": "Name changed via demo!",
    "avatar": "https://umnico.com/api/files/download/25521.jpg?sa=41950&fileId=AgACAgIAAxUAAWMEaBhE7CvUZAU1y79lcZzFc-SqAALNvTEb6bGwSwwjU4lZHZKkAQADAgADYgADKQQ",
    "email": "newemail@september.ninth",
    "phone": "8 999 999 99 99",
    "address": "Kumbala, 23",
    "profiles": [
        {
            "id": 19825633,
            "login": "Bogdan Shipilov",
            "type": "telebot",
            "socialId": "1649914428",
            "profileUrl": "https://t.me/theBodyan"
        }
    ]
}',
                true
            )
        )->times(1);
        $apiCommands->expects('getLead')->with(self::LEAD_ID)->andReturn(
            json_decode(
                '{"userId":"fakeid"}',
                true
            )
        );
        $appHandler->expects('getApiCommands')->andReturn(
            $apiCommands
        )->times(2);
        $data = m::mock(\BmPlatform\Umnico\Utils\DataWrap::class, [$appHandler, json_decode($data, true)]);
        $data->expects('timestamp')->andReturn($this->mockTimestamp())->once();
        $data->makePartial();

        $eventHandler = new MessageIncoming();
        $eventHandlerData = $eventHandler($data);
        $this->assertEquals($event->message, $eventHandlerData->message);
        $this->assertEquals($event->participant, $eventHandlerData->participant);
    }

    public function dataProvider(): array
    {
        return [
            'basic inbox' => [
                '{
    "accountId": 1607,
    "leadId": "' . self::LEAD_ID . '",
    "type": "message.incoming",
    "message": {
        "messageId": "' . self::MESSAGE_ID . '",
        "datetime": "2022-09-13T08:56:25.000Z",
        "sa": {
            "id": "'.self::INTEGRATION_ID.'",
            "type": "telebot",
            "login": "idontlikepizza",
            "avatar": null
        },
        "message": {
            "text": "no data"
        },
        "incoming": true,
        "sender": {
            "id": 19825633,
            "customerId": "' . self::CONTACT_ID . '",
            "login": "Bogdan Shipilov",
            "avatar": "https://umnico.com/api/files/download/25521.jpg?sa=41950&fileId=AgACAgIAAxUAAWMEaBhE7CvUZAU1y79lcZzFc-SqAALNvTEb6bGwSwwjU4lZHZKkAQADAgADYgADKQQ",
            "type": "telebot",
            "socialId": "1649914428",
            "profileUrl": "https://t.me/theBodyan"
        },
        "source": {
            "id": "' . self::SOURCE_ID . '",
            "realId": ' . 27330686 . ',
            "name": null,
            "type": "message",
            "saId": "'.self::INTEGRATION_ID.'",
            "sender": "' . self::SOURCE_IDENTIFIER . '",
            "token": null,
            "identifier": "' . self::SOURCE_IDENTIFIER . '"
        },
        "replyTo": null
    }
}',
                new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: self::CONTACT_ID,
                        name: 'Name changed via demo!',
                        phone: '8 999 999 99 99',
                        email: 'newemail@september.ninth',
                        avatarUrl: 'https://umnico.com/api/files/download/25521.jpg?sa=41950&fileId=AgACAgIAAxUAAWMEaBhE7CvUZAU1y79lcZzFc-SqAALNvTEb6bGwSwwjU4lZHZKkAQADAgADYgADKQQ',
                        extraFields: ['address' => 'Kumbala, 23'],
                        messengerId: null,
                        extraData: Helpers::withoutEmptyStrings([
                            ExtraDataProps::CONTACT_LOGIN => 'Bogdan Shipilov',
                            ExtraDataProps::CONTACT_PROFILES => json_decode(
                                '[
        {
            "id": 19825633,
            "login": "Bogdan Shipilov",
            "type": "telebot",
            "socialId": "1649914428",
            "profileUrl": "https://t.me/theBodyan"
        }
    ]',
                                true
                            )
                        ])

                    ),
                    message: new MessageData(
                        externalId: self::MESSAGE_EXTERNAL_ID,
                        text: 'no data',
                        attachments: null,
                        date: Carbon::parse('2022-09-13T08:56:25.000Z'),
                        extraData: [
                            \BmPlatform\Umnico\Utils\ExtraDataProps::CHAT_EXTERNAL_ID => self::SOURCE_EXTERNAL_ID
                        ]
                    ),
                    externalItem: null,
                    flags: null,
                    timestamp: $this->mockTimestamp(),
                ),
            ],

            'photo' => [
                '{
    "accountId": 1607,
    "leadId": "' . self::LEAD_ID . '",
    "type": "message.incoming",
    "message": {
        "messageId": "' . self::MESSAGE_ID . '",
        "datetime": "2022-09-13T08:59:16.000Z",
        "sa": {
            "id": "'.self::INTEGRATION_ID.'",
            "type": "telebot",
            "login": "idontlikepizza",
            "avatar": null
        },
        "message": {
            "text": "photo attachment",
            "attachments": [
                {
                    "type": "photo",
                    "url": "https://umnico.com/api/files/download/46307.jpg?sa=42491&fileId=AgACAgIAAxkBAAMmYyBGZJndKpEoNUmpu3uSTaTtlmUAAi7CMRvtCQFJDJzU13eOR7wBAAMCAAN5AAMpBA",
                    "preview": "https://umnico.com/api/files/download/602.jpg?sa=42491&fileId=AgACAgIAAxkBAAMmYyBGZJndKpEoNUmpu3uSTaTtlmUAAi7CMRvtCQFJDJzU13eOR7wBAAMCAANzAAMpBA",
                    "filesize": 46307
                }
            ]
        },
        "incoming": true,
        "sender": {
            "id": 19825633,
            "customerId": "' . self::CONTACT_ID . '",
            "login": "Bogdan Shipilov",
            "avatar": "https://umnico.com/api/files/download/25521.jpg?sa=41950&fileId=AgACAgIAAxUAAWMEaBhE7CvUZAU1y79lcZzFc-SqAALNvTEb6bGwSwwjU4lZHZKkAQADAgADYgADKQQ",
            "type": "telebot",
            "socialId": "1649914428",
            "profileUrl": "https://t.me/theBodyan"
        },
        "source": {
            "id": "' . self::SOURCE_ID . '",
            "realId": ' . 27330686 . ',
            "name": null,
            "type": "message",
            "saId": "'.self::INTEGRATION_ID.'",
            "sender": "' . self::SOURCE_IDENTIFIER . '",
            "token": null,
            "identifier": "' . self::SOURCE_IDENTIFIER . '"
        },
        "replyTo": null
    }
}',
                new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: self::CONTACT_ID,
                        name: 'Name changed via demo!',
                        phone: '8 999 999 99 99',
                        email: 'newemail@september.ninth',
                        avatarUrl: 'https://umnico.com/api/files/download/25521.jpg?sa=41950&fileId=AgACAgIAAxUAAWMEaBhE7CvUZAU1y79lcZzFc-SqAALNvTEb6bGwSwwjU4lZHZKkAQADAgADYgADKQQ',
                        extraFields: ['address' => 'Kumbala, 23'],
                        messengerId: null,
                        extraData: Helpers::withoutEmptyStrings([
                            ExtraDataProps::CONTACT_LOGIN => 'Bogdan Shipilov',
                            ExtraDataProps::CONTACT_PROFILES => json_decode(
                                '[
        {
            "id": 19825633,
            "login": "Bogdan Shipilov",
            "type": "telebot",
            "socialId": "1649914428",
            "profileUrl": "https://t.me/theBodyan"
        }
    ]',
                                true
                            )
                        ])

                    ),
                    message: new MessageData(
                        externalId: self::MESSAGE_EXTERNAL_ID,
                        text: 'photo attachment',
                        attachments: [
                            new MediaFile(
                                MediaFileType::Image,
                                'https://umnico.com/api/files/download/46307.jpg?sa=42491&fileId=AgACAgIAAxkBAAMmYyBGZJndKpEoNUmpu3uSTaTtlmUAAi7CMRvtCQFJDJzU13eOR7wBAAMCAAN5AAMpBA'
                            )
                        ],
                        date: Carbon::parse('2022-09-13T08:59:16.000Z'),
                        extraData: [
                            \BmPlatform\Umnico\Utils\ExtraDataProps::CHAT_EXTERNAL_ID => self::SOURCE_EXTERNAL_ID
                        ]
                    ),
                    externalItem: null,
                    flags: null,
                    timestamp: $this->mockTimestamp(),
                ),
            ],
        ];
    }

    public function mockAppInstance(): AppInstance
    {
        static $value;

        return $value ?? $value = m::mock(AppInstance::class);
    }

    public function mockChat()
    {
        static $chat;


        if (isset($chat)) {
            return $chat;
        }

        return $chat = new Chat('chat', self::SOURCE_EXTERNAL_ID, self::CONTACT_ID, self::MANAGER_ID,);
    }

    public function mockTimestamp()
    {
        static $ts;

        if (isset($ts)) {
            return $ts;
        }

        return $ts = new Carbon();
    }
}
