<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Events\OutboxSent;
use Carbon\Carbon;
use Mockery as m;

class UOutboxTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testOutbox()
    {
        $dataText = '{
    "accountId": 1607,
    "leadId": 20920564,
    "type": "message.outgoing",
    "message": {
        "customId":"umnico-web-client-123",
        "messageId": 5750360614529025000,
        "datetime": 1663050583243,
        "sa": {
            "id": 42202,
            "type": "viber_bot",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000"
        },
        "message": {
            "text": "text"
        },
        "incoming": false,
        "sender": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000",
            "type": "viber_bot",
            "socialId": null
        },
        "source": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "realId": 27330780,
            "name": null,
            "type": "message",
            "saId": 42202,
            "sender": "/mXOBOxeQLIaKhvGq6/+gA==",
            "token": null,
            "identifier": "4fbce4e56027e79a-24479e9416db5de8-bd28f35b93ae4b74",
            "expires": null
        }
    }
}';
        $dataTextWithCustomId = '{
    "accountId": 1607,
    "leadId": 20920564,
    "type": "message.outgoing",
    "message": {
        "customId": "UmnicoCustomId8818238712731839",
        "messageId": 5750360397800951000,
        "datetime": 1663050531587,
        "sa": {
            "id": 42202,
            "type": "viber_bot",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000"
        },
        "message": {
            "text": "text"
        },
        "incoming": false,
        "sender": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "login": "BodyanWaTestBot",
            "avatar": "https://media-direct.cdn.viber.com/pg_download?pgtp=icons&dlid=0-04-01-67085dd57d87e594a5b678207cae239430900570657cc7815bee2c176f9619f7&fltp=jpg&imsz=0000",
            "type": "viber_bot",
            "socialId": null
        },
        "source": {
            "id": "/mXOBOxeQLIaKhvGq6/+gA==",
            "realId": 27330780,
            "name": null,
            "type": "message",
            "saId": 42202,
            "sender": "/mXOBOxeQLIaKhvGq6/+gA==",
            "token": null,
            "identifier": "4fbce4e56027e79a-24479e9416db5de8-bd28f35b93ae4b74",
            "expires": null
        }
    }
}';
        $appHandler = m::mock(\BmPlatform\Umnico\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $apiCommands = m::mock(new \BmPlatform\Umnico\ApiCommands(new \BmPlatform\Umnico\ApiClient('domain', 'token')));
        $apiCommands->expects('getLead')->with(20920564)->andReturn(['userId' => '121312', 'customerId' => '12312312312'])->times(1);
        $appHandler->expects('getApiCommands')->andReturn(
            $apiCommands
        )->times(1);
        $data = m::mock(\BmPlatform\Umnico\Utils\DataWrap::class, [
            $appHandler,
            json_decode($dataText, true)
        ]);

        $data->expects('timestamp')->andReturn($ts = new Carbon())->zeroOrMoreTimes();
        $data->makePartial();

        \Illuminate\Support\Facades\Config::shouldReceive('get')->with('umnico_params.custom_id')->andReturn(
            'UmnicoCustomId8818238712731839'
        )->zeroOrMoreTimes();
        $outbox = new \BmPlatform\Umnico\EventHandlers\MessageOutgoing();

        $this->assertEquals(
            new OutboxSent(
                chat: $data->source(),
                message: new MessageData(
                    externalId: 20920564 . ':' . 5750360614529025000,
                    text: 'text',
                    attachments: null,
                    date: Carbon::parse(1663050583243),
                    extraData: [\BmPlatform\Umnico\Utils\ExtraDataProps::CHAT_EXTERNAL_ID => $data->sourceInstance()],
                ),
//                operator: '121312',
                timestamp: $data->timestamp(),
            ),
            $outbox($data)
        );

        $data = m::mock(\BmPlatform\Umnico\Utils\DataWrap::class, [
            m::mock(\BmPlatform\Umnico\AppHandler::class, [
                $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
                m::mock(\Illuminate\Contracts\Config\Repository::class),
            ]),
            json_decode($dataTextWithCustomId, true)
        ]);
        $data->makePartial();

        $outbox = new \BmPlatform\Umnico\EventHandlers\MessageOutgoing();

        $this->assertEquals(
            null,
            $outbox($data)
        );
    }
}
